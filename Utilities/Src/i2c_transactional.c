/*
 * i2c_transactional.c
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */


#include <stddef.h>
#include "main.h"
#include "../Inc/i2c_transactional.h"


#define I2C_QUEUE_SIZE  100

#define I2C_BUFFER_MAX_SIZE 12

#define I2C_INTERFACES  2
#define INTERFACE_I2C_0_INDEX    0
#define INTERFACE_I2C_1_INDEX    1
#define INTERFACE_I2C_NONE_INDEX 0

static I2C_HandleTypeDef *i2c_handle_ptr[I2C_INTERFACES];
static DMA_HandleTypeDef *i2c_dmarx_handle_ptr[I2C_INTERFACES];
static DMA_HandleTypeDef *i2c_dmatx_handle_ptr[I2C_INTERFACES];




typedef enum
{
	I2C_NONE,
	I2C_READ,
	I2C_WRITE
}
i2c_transfer_type_t;

typedef enum
{
	I2C_TRANSFER_NONE,
	I2C_TRANSFER_SCHEDULED,
	I2C_TRANSFER_ONGOING,
	I2C_TRANSFER_END
}
i2c_transfer_status_t;


typedef struct __attribute__((packed, aligned(4)))
{
	uint8_t reg_addr;
	uint8_t data[I2C_BUFFER_MAX_SIZE];
}i2c_buffer_data_t;

typedef union
{
	i2c_buffer_data_t elements;
	uint8_t           all[I2C_BUFFER_MAX_SIZE+1];
}i2c_buffer_t;

typedef struct __attribute__((packed, aligned(4)))
{
	//i2c_buffer_t          write_buffer;
	//uint8_t               *read_buffer;

	uint8_t               write_data[I2C_BUFFER_MAX_SIZE];
	uint32_t              write_size;
	uint8_t               *read_data;
	uint32_t              read_size;
	i2c_transfer_type_t   type;
	i2c_transfer_status_t status;
	ic_address_t          device;
	callback_t            callback;
	void                  *p_user_data;
	uint32_t              counter;
	ids_t                 id;
}i2c_transaction_t;





static i2c_transaction_t i2c_transactions[I2C_INTERFACES][I2C_QUEUE_SIZE];   //TODO: use 1-dim table, because we have .device member


static uint32_t i2c_interface_translation(interface_t interface)
{
	switch (interface)
	{
		case INTERFACE_I2C_1:	return INTERFACE_I2C_0_INDEX;
		case INTERFACE_I2C_2:   return INTERFACE_I2C_1_INDEX;
		default:                return INTERFACE_I2C_NONE_INDEX;
	}

	return INTERFACE_I2C_NONE_INDEX;
}



void i2c_trans_init(interface_t interface,
		            I2C_HandleTypeDef *i2c_handle,
					DMA_HandleTypeDef *i2c_dmarx_handle,
					DMA_HandleTypeDef *i2c_dmatx_handle)
{
	uint32_t k;
	k = i2c_interface_translation(interface);

	for (uint32_t i=0;i<I2C_QUEUE_SIZE; i++)
	{
		for (uint32_t j=0; j<I2C_BUFFER_MAX_SIZE; j++)
		{
			i2c_transactions[k][i].write_data[j] = 0;
		}

		i2c_transactions[k][i].read_data   = NULL;
		i2c_transactions[k][i].write_size  = 0;
		i2c_transactions[k][i].read_size   = 0;
		i2c_transactions[k][i].type        = I2C_NONE;
		i2c_transactions[k][i].status      = I2C_TRANSFER_NONE;
		i2c_transactions[k][i].p_user_data = NULL;
		i2c_transactions[k][i].callback    = NULL;
		i2c_transactions[k][i].counter     = 0;
	}

	i2c_handle_ptr[k]       = i2c_handle;
	i2c_dmarx_handle_ptr[k] = i2c_dmarx_handle;
	i2c_dmatx_handle_ptr[k] = i2c_dmatx_handle;
}


static i2c_transaction_t *get_oldest_transaction(interface_t interface, i2c_transfer_status_t status)
{
	i2c_transaction_t *ret_val = NULL;
	uint32_t k;

	k = i2c_interface_translation(interface);

	//find element with smallest counter
	uint32_t min_counter = (0-1);
	uint32_t element_with_min_counter = I2C_QUEUE_SIZE;
	for (uint32_t i=0;i<I2C_QUEUE_SIZE; i++)
	{
		if (i2c_transactions[k][i].status == status)
		{
			if (i2c_transactions[k][i].counter < min_counter)
			{
				min_counter = i2c_transactions[k][i].counter;
				element_with_min_counter = i;
			}
		}
	}

	if (element_with_min_counter < I2C_QUEUE_SIZE)
	{
		ret_val = &(i2c_transactions[k][element_with_min_counter]);
	}

	return ret_val;
}



static i2c_transaction_t *get_oldest_scheduled_transaction(interface_t interface)
{
	return get_oldest_transaction(interface, I2C_TRANSFER_SCHEDULED);
}

static i2c_transaction_t *get_oldest_ongoing_transaction(interface_t interface)
{
	return get_oldest_transaction(interface, I2C_TRANSFER_ONGOING);
}


static err_code_t put_i2c_queue(ids_t id,
		                        ic_address_t device,
		                        i2c_transfer_type_t type,
								uint8_t *write_data,
								uint32_t write_size,
								uint8_t *read_data,
								uint32_t read_size,
								callback_t callback,
								void *p_user_data)
{

	err_code_t err_code = RESULT_ERROR;
	uint32_t k;

	k = i2c_interface_translation(device.interface);


	//check inputs
	if (type == I2C_WRITE)
	{
		if (write_data == NULL)
		{
			art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
			return RESULT_BAD_PARAMETER;
		}

		if (write_size > I2C_BUFFER_MAX_SIZE)
		{
			art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
			return RESULT_BAD_PARAMETER;
		}

		if (read_data != NULL)
		{
			art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
			return RESULT_BAD_PARAMETER;
		}
	}
	else if (type == I2C_READ)
	{
		if (write_data != NULL)
		{
			if (write_size > I2C_BUFFER_MAX_SIZE)
			{
				art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
				return RESULT_BAD_PARAMETER;
			}
		}

		if (read_data == NULL)
		{
			art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
			return RESULT_BAD_PARAMETER;
		}

		if (read_size == 0)
		{
			art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
			return RESULT_BAD_PARAMETER;
		}
	}
	else
	{
		art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
		return RESULT_BAD_PARAMETER;
	}


	//find max counter
	uint32_t max_counter = 0;
	for (uint32_t i=0;i<I2C_QUEUE_SIZE; i++)
	{
		if (i2c_transactions[k][i].counter > max_counter)
		{
			max_counter = i2c_transactions[k][i].counter;
		}
	}

	//find free slot
	for (uint32_t i=0;i<I2C_QUEUE_SIZE; i++)
	{
		if (i2c_transactions[k][i].status == I2C_TRANSFER_NONE)
		{
			//add transfer
			if (write_data != NULL)
			{
				for (uint32_t j=0; j<write_size; j++)
				{
					i2c_transactions[k][i].write_data[j] = write_data[j];
				}
			}

			i2c_transactions[k][i].read_data   = read_data;
			i2c_transactions[k][i].device      = device;
			i2c_transactions[k][i].type        = type;
			i2c_transactions[k][i].write_size  = write_size;
			i2c_transactions[k][i].read_size   = read_size;
			i2c_transactions[k][i].callback    = callback;
			i2c_transactions[k][i].p_user_data = p_user_data;
			i2c_transactions[k][i].counter     = max_counter + 1;
			i2c_transactions[k][i].status      = I2C_TRANSFER_SCHEDULED;
			i2c_transactions[k][i].id          = id;
			err_code = RESULT_OK;
			break;
		}
	}

	return err_code;
}

err_code_t i2c_trans_put_read_A(ids_t id,
								ic_address_t device,
		                        uint8_t *write_data,
							    uint32_t write_size,
							    uint8_t *read_data,
							    uint32_t read_size,
							    callback_t callback,
							    void *p_user_data)
{
	return put_i2c_queue(id, device, I2C_READ, write_data, write_size, read_data, read_size, callback, p_user_data);
}

err_code_t i2c_trans_put_read_B(ids_t id,
		                        ic_address_t device,
		                        uint8_t *write_data,
							    uint32_t write_size,
							    uint8_t *read_data,
							    uint32_t read_size,
							    callback_t callback,
							    void *p_user_data)
{
	err_code_t err_code = RESULT_ERROR;

	err_code = put_i2c_queue(id, device, I2C_WRITE, write_data, write_size, NULL, 0, callback, p_user_data);

	if (err_code == RESULT_OK)
	{
		err_code = put_i2c_queue(id, device, I2C_READ, NULL, 0, read_data, read_size, callback, p_user_data);
	}

	return err_code;
}


err_code_t i2c_trans_put_write(ids_t id, ic_address_t device, uint8_t *write_data, uint32_t write_size, callback_t callback, void *p_user_data)
{
	return put_i2c_queue(id, device, I2C_WRITE, write_data, write_size, NULL, 0, callback, p_user_data);
}


err_code_t i2c_trans_flush(interface_t interface)
{
	err_code_t err_code = RESULT_ERROR;

	do
	{
		err_code = i2c_trans_execute(interface);
	}
	while ((err_code != RESULT_ERROR) && (err_code != RESULT_IDLE));

	if (err_code == RESULT_IDLE)
	{
		err_code = RESULT_OK;
	}

	return err_code;
}



#define I2C_DMA      0
#define I2C_POLLING  1


void i2c_trans_info(interface_t interface)
{
	uint32_t k;
	k = i2c_interface_translation(interface);

	for (uint32_t i=0;i<I2C_QUEUE_SIZE; i++)
	{
		//for (uint32_t j=0; j<I2C_BUFFER_MAX_SIZE; j++)
		//{
		//	i2c_transactions[k][i].write_data[j] = 0;
		//}

		//i2c_transactions[k][i].read_data   = NULL;
		//i2c_transactions[k][i].write_size  = 0;
		//i2c_transactions[k][i].read_size   = 0;
		//i2c_transactions[k][i].type        = I2C_NONE;

		art_printf("i2c %d / %d : status=%d, counter=%d \r\n",k, i, i2c_transactions[k][i].status, i2c_transactions[k][i].counter);
		//i2c_transactions[k][i].p_user_data = NULL;
		//i2c_transactions[k][i].callback    = NULL;
		//i2c_transactions[k][i].counter     = 0;
	}
}


err_code_t i2c_trans_execute(interface_t interface)
{
	err_code_t err_code = RESULT_ERROR;
	uint32_t k = i2c_interface_translation(interface);
	uint8_t nbr_of_tries = 0;
	#define NBR_OF_TRIES 10



	#if I2C_DMA
	/* Wait for the end of the transfer */
	/*  Before starting a new communication transfer, you need to check the current
	    state of the peripheral; if it�s busy you need to wait for the end of current
	    transfer before starting a new one.
	*/
	if (HAL_I2C_GetState(i2c_handle_ptr[k]) != HAL_I2C_STATE_READY)
    {
		err_code = RESULT_BUSY;
		return err_code;
	}
	#endif

	i2c_transaction_t *transaction = NULL;

	//remove last transfer
	transaction = get_oldest_ongoing_transaction(interface);
	if (transaction != NULL)
	{
		transaction->status = I2C_TRANSFER_NONE;
	}

	// start new transfer

	transaction = get_oldest_scheduled_transaction(interface);
	if (transaction != NULL)
	{
		  /* Start the transmission process */
		if (transaction->type == I2C_WRITE)
		{
			  /* While the I2C in reception process, user can transmit data through
			     "aTxBuffer" buffer */
			nbr_of_tries = 0;
			#if I2C_DMA
			while(HAL_I2C_Master_Transmit_DMA(i2c_handle_ptr[k],
											  (uint16_t)(transaction->device.address),
											  transaction->write_data,
											  transaction->write_size) != HAL_OK)
			#endif
			#if I2C_POLLING
			while(HAL_I2C_Master_Transmit(i2c_handle_ptr[k],
										(uint16_t)(transaction->device.address),
										transaction->write_data,
										transaction->write_size,
										1000) != HAL_OK)
			#endif
			{
				/* Error_Handler() function is called when Timout error occurs.
				   When Acknowledge failure ocucurs (Slave don't acknowledge it's address)
				   Master restarts communication */
				if (HAL_I2C_GetError(i2c_handle_ptr[k]) != HAL_I2C_ERROR_AF)
				{
					Error_Handler();
				}

				if (++nbr_of_tries > NBR_OF_TRIES)
				{
					err_code = RESULT_ERROR_LAST + HAL_I2C_GetError(i2c_handle_ptr[k]);
					return err_code;
				}
			}
		}
		else if (transaction->type == I2C_READ)
		{
			if ((transaction->write_data != NULL) && (transaction->write_size > 0))
			{
				nbr_of_tries = 0;
				#if I2C_DMA
				while(HAL_I2C_Master_Transmit_DMA(i2c_handle_ptr[k],
												  (uint16_t)(transaction->device.address),
												  transaction->write_data,
												  transaction->write_size) != HAL_OK)
				#endif
				#if I2C_POLLING
				while(HAL_I2C_Master_Transmit(i2c_handle_ptr[k],
											  (uint16_t)(transaction->device.address),
											  transaction->write_data,
											  transaction->write_size,
											  1000) != HAL_OK)
				#endif
				{
					/* Error_Handler() function is called when Timout error occurs.
					   When Acknowledge failure ocucurs (Slave don't acknowledge it's address)
					   Master restarts communication */
					if (HAL_I2C_GetError(i2c_handle_ptr[k]) != HAL_I2C_ERROR_AF)
					{
						Error_Handler();
					}

					if (++nbr_of_tries > NBR_OF_TRIES)
					{
						err_code = RESULT_ERROR_LAST + HAL_I2C_GetError(i2c_handle_ptr[k]);
						return err_code;
					}
				}

				#if I2C_DMA
				if (HAL_I2C_GetState(i2c_handle_ptr[k]) != HAL_I2C_STATE_READY)
				{
					err_code = RESULT_BUSY;
					return err_code;
				}
				#endif
			}

			nbr_of_tries = 0;
			#if I2C_DMA
			while(HAL_I2C_Master_Receive_DMA(i2c_handle_ptr[k],
                    					     (uint16_t)(transaction->device.address),
											 transaction->read_data,
											 transaction->read_size) != HAL_OK)
			#endif
			#if I2C_POLLING
			while(HAL_I2C_Master_Receive(i2c_handle_ptr[k],
					                     (uint16_t)(transaction->device.address),
										 transaction->read_data,
										 transaction->read_size,
										 1000) != HAL_OK)
			#endif
 		    {
			    /* Error_Handler() function is called when Timout error occurs.
			       When Acknowledge failure ocucurs (Slave don't acknowledge it's address)
			       Master restarts communication */
			    if (HAL_I2C_GetError(i2c_handle_ptr[k]) != HAL_I2C_ERROR_AF)
			    {
			        Error_Handler();
			    }

				if (++nbr_of_tries > NBR_OF_TRIES)
				{
					err_code = RESULT_ERROR_LAST + HAL_I2C_GetError(i2c_handle_ptr[k]);
					return err_code;
				}
			}
		}

		transaction->status = I2C_TRANSFER_ONGOING;
		err_code = RESULT_OK;

		#if 0
		art_printf("[%s] (%d) type=%d ",__func__,__LINE__,transaction->type);
		art_printf("WR = ");
		for (int i=0; i<transaction->write_size; i++)
		{
			art_printf("0x%02X ",transaction->write_data[i]);
		}
		art_printf("RD = ");
		for (int i=0; i<transaction->read_size; i++)
		{
			art_printf("0x%02X ",transaction->read_data[i]);
		}
		art_printf("\r\n");
		#endif
	}
	else
	{
		err_code = RESULT_IDLE;
	}

	return err_code;
}



