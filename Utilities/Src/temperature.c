/*
 * temperature.c
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */

#include "../Inc/temperature.h"
#include "../../Devices/Inc/at30ts74.h"
#include "ic_devices.h"

err_code_t read_temperature(float *temperature)
{
#if TEMPERATURE_PRESENT
	return at30ts74_ReadTemperature(at30ts74_addr, temperature);
#else
	*temperature = 0.0;
	return RESULT_ERROR;
#endif
}
