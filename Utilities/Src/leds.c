/*
 * leds.c
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */

#include <math.h>
#include "ic_devices.h"
#include "../Inc/leds.h"
#include "../Inc/math_utils.h"
#include "../../Devices/Inc/lp5024.h"
#include "main.h"



typedef struct
{
	uint8_t r_ch;
	uint8_t g_ch;
	uint8_t b_ch;
}rgb_map_t;


typedef struct
{
	ic_address_t *device_addr;
	uint8_t      channel;
	rgb_map_t    rgb_map;
	led_color_t  color;
	uint8_t      priority;   // 0 - lowest
}leds_definition_t;


typedef struct
{
	uint8_t  pattern_number;
	uint16_t duration;			// in [100ms] units
}scene_element_t;

typedef struct
{
	uint8_t         active_scene_elements;
	scene_element_t scene_elements[SCENE_PATTERN_COUNT];
}scenes_t;



typedef struct
{
	TIM_HandleTypeDef *handle_ptr;
	uint16_t pwm_channel;
} tim_def_t;

#define PWM_CHANNELS 2
static tim_def_t tim_def[PWM_CHANNELS];


static uint8_t active_scene_number = 0;
static bool play_scene_indicator = false;
static uint32_t base_frequency = 0;


static uint8_t lut_gamma_r[256];
static uint8_t lut_gamma_g[256];
static uint8_t lut_gamma_b[256];

static scenes_t scenes[SCENES_COUNT] =
{
		//scene 0
		{
				.active_scene_elements = 2,
				.scene_elements =
				{
						{ .pattern_number = 0, .duration = 30 },  // scene 0, element 0
						{ .pattern_number = 1, .duration = 30 },  // scene 0, element 1
						{ .pattern_number = 0, .duration = 0  },  // scene 0, element 2
						{ .pattern_number = 0, .duration = 0  }   // scene 0, element 3
				}
		},

		//scene 1
		{
				.active_scene_elements = 0,
				.scene_elements =
				{
						{ .pattern_number = 0, .duration = 0 },  // scene 1, element 0
						{ .pattern_number = 0, .duration = 0 },  // scene 1, element 1
						{ .pattern_number = 0, .duration = 0 },  // scene 1, element 2
						{ .pattern_number = 0, .duration = 0 }   // scene 1, element 3
				}
		},

		//scene 2
		{
				.active_scene_elements = 0,
				.scene_elements =
				{
						{ .pattern_number = 0, .duration = 0 },  // scene 2, element 0
						{ .pattern_number = 0, .duration = 0 },  // scene 2, element 1
						{ .pattern_number = 0, .duration = 0 },  // scene 2, element 2
						{ .pattern_number = 0, .duration = 0 }   // scene 2, element 3
				}
		},

		//scene 3
		{
				.active_scene_elements = 0,
				.scene_elements =
				{
						{ .pattern_number = 0, .duration = 0 },  // scene 3, element 0
						{ .pattern_number = 0, .duration = 0 },  // scene 3, element 1
						{ .pattern_number = 0, .duration = 0 },  // scene 3, element 2
						{ .pattern_number = 0, .duration = 0 }   // scene 3, element 3
				}
		}
};

static pattern_t led_patterns[LEDS_PATTERNS_COUNT] =
{
		// pattern 0
		{
				.color =
				{
					#if LED_GROUP_PRESENT_0
					{150,   0,   0},	{120,   0,   0},	{ 70,   0,   0},	{  50, 10,   0},	//channels 0..3
					{ 30,  20,   0},	{ 10,   35,  0},	{  0,  50,   0},	{  0,  50,   0},	//channels 4..7
					#endif
					#if LED_GROUP_PRESENT_1
					{  0,  40,  20},	{ 10,  40,  20},    { 80,  40,  20},    {150,  40,  20},	//channels 8..11
					{ 80,  45,  15},	{ 50,  50,  10},    {  0,  75,   5},    {  0,  90,   0},	//channels 12..15
					#endif
					#if LED_GROUP_PRESENT_2
					{ 40,  90,   0},	{100,  75,   5},    {150,  50,  10},    {100,  45,  15},	//channels 16..19
					{100,  40,  20},	{ 80,  40,  20},    { 40,  40,  20},    {  0,  40,  90},	//channels 20..23
					#endif
					#if LED_GROUP_PRESENT_3
					{  0,  50,   0},	{  0,  50,   0},    { 10,  35,   0},    { 30,  20,   0},	//channels 24..27
					{ 50,  10,   0},	{ 70,   0,   0},    {120,   0,   0},    {150,   0,   0},	//channels 28..31
					#endif
					#if LED_GROUP_PRESENT_4
					{ 150,   0,   0},	{120,   0,   0},	{ 70,   0,   0},	{ 50,  10,   0},	//channels 32..35
					{ 30,  20,   0},	{ 10,  35,   0},	{  0,  50,   0},	{  0,  50,   0},	//channels 36..39
					#endif
					#if LED_GROUP_PRESENT_5
					{  0,  40,  90},	{ 40,  40,  20},    { 80,  40,  20},    { 40,  40,  20},	//channels 40..43
					{100,  45,  15},	{150,  50,  10},    {100,  75,   5},    { 40,  90,   0},	//channels 44..47
					#endif
					#if LED_GROUP_PRESENT_6
					{ 80,  90,   0},	{ 50,  75,   5},    {  0,  50,  10},    {  0,  45,  15},	//channels 48..51
					{  0,  40,  20},	{ 10,  40,  20},    { 80,  40,  20},    {150,  40,  20},	//channels 52..55
					#endif
					#if LED_GROUP_PRESENT_7
					{  0,  50,   0},	{  0,  50,   0},    { 10,  35,   0},    { 30,  20,   0},	//channels 56..59
					{ 50,  10,   0},	{ 70,   0,   0},    {120,   0,   0},    {150,   0,   0} 	//channels 60..63
					#endif
				}
		},

		// pattern 1
		{
				.color =
				{
					#if LED_GROUP_PRESENT_0
				    { 0 ,   0, 100},	{  5,  0 ,  90},	{10,    0,  50},    {40,   0 ,  40},	//channels 0..3
					{70,   0 ,  20},	{140,  30,  20},	{150,  90,  10},	{160,  100,  0},	//channels 4..7
					#endif
					#if LED_GROUP_PRESENT_1
					{ 50, 200,   0},	{ 40, 210,   0},    { 30, 180,   0},    { 20,  150,   0},	//channels 8..11
					{ 10, 90,   10},	{ 40, 50,   20},    { 70, 20,   10},    {100,  0,     0},	//channels 12..15
					#endif
					#if LED_GROUP_PRESENT_2
					{100,   0,   0},	{ 70,  20,  10},    { 40,  50,  20},    { 10,  90,  10},	//channels 16..19
					{ 20, 150,   0},	{ 30, 180,   0},    { 40, 210,   0},    { 50, 200,   0},	//channels 20..23
					#endif
					#if LED_GROUP_PRESENT_3
					{160, 100,   0},	{150,  90,  10},    {140,  30,  20},    { 70,   0,  20},	//channels 24..27
					{ 40,   0,  40},	{ 10,   0,  50},    {  5,   0,  90},    {  0,   0, 100},	//channels 28..31
					#endif
					#if LED_GROUP_PRESENT_4
					{  0,   0, 100},	{  5,   0,  90},	{ 10,   0,  50},	{ 40,   0,  40},	//channels 32..35
					{ 70,   0,  20},	{140,  30,  20},	{150,  90,  10},	{160, 100,   0},	//channels 36..39
					#endif
					#if LED_GROUP_PRESENT_5
					{ 50, 200,   0},	{ 40, 210,   0},    { 30, 180,   0},    { 20, 150,   0},	//channels 40..43
					{ 10,  90,  10},	{ 40,  50,  20},    { 70,  20,  10},    {100,   0,   0},	//channels 44..47
					#endif
					#if LED_GROUP_PRESENT_6
					{100,   0,   0},	{ 70,  20,  10},    { 40,  50,  20},    { 10,  90,  10},	//channels 48..51
					{ 20, 150,   0},	{ 30, 180,   0},    { 40, 210,   0},    { 50, 200,   0},	//channels 52..55
					#endif
					#if LED_GROUP_PRESENT_7
					{160, 100,   0},	{150,  90,  10},    {140,  30,  20},    { 70,   0,  20},	//channels 56..59
					{40,   0,   40},	{ 10,   0,  50},    { 5,   0,   90},    {  0,   0, 100} 	//channels 60..63
					#endif
				}
		},

		// pattern 2
		{
				.color =
				{
					#if LED_GROUP_PRESENT_0
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 0..3
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 4..7
					#endif
					#if LED_GROUP_PRESENT_1
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 8..11
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 12..15
					#endif
					#if LED_GROUP_PRESENT_2
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 16..19
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 20..23
					#endif
					#if LED_GROUP_PRESENT_3
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 24..27
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 28..31
					#endif
					#if LED_GROUP_PRESENT_4
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 32..35
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 36..39
					#endif
					#if LED_GROUP_PRESENT_5
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 40..43
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 44..47
					#endif
					#if LED_GROUP_PRESENT_6
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 48..51
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 52..55
					#endif
					#if LED_GROUP_PRESENT_7
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 56..59
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0} 	//channels 60..63
					#endif
				}
		},

		// pattern 3
		{
				.color =
				{
					#if LED_GROUP_PRESENT_0
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 0..3
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 4..7
					#endif
					#if LED_GROUP_PRESENT_1
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 8..11
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 12..15
					#endif
					#if LED_GROUP_PRESENT_2
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 16..19
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 20..23
					#endif
					#if LED_GROUP_PRESENT_3
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 24..27
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 28..31
					#endif
					#if LED_GROUP_PRESENT_4
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 32..35
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 36..39
					#endif
					#if LED_GROUP_PRESENT_5
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 40..43
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 44..47
					#endif
					#if LED_GROUP_PRESENT_6
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 48..51
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 52..55
					#endif
					#if LED_GROUP_PRESENT_7
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 56..59
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0} 	//channels 60..63
					#endif
				}
		},

		// pattern 4
		{
				.color =
				{
					#if LED_GROUP_PRESENT_0
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 0..3
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 4..7
					#endif
					#if LED_GROUP_PRESENT_1
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 8..11
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 12..15
					#endif
					#if LED_GROUP_PRESENT_2
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 16..19
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 20..23
					#endif
					#if LED_GROUP_PRESENT_3
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 24..27
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 28..31
					#endif
					#if LED_GROUP_PRESENT_4
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 32..35
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 36..39
					#endif
					#if LED_GROUP_PRESENT_5
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 40..43
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 44..47
					#endif
					#if LED_GROUP_PRESENT_6
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 48..51
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 52..55
					#endif
					#if LED_GROUP_PRESENT_7
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 56..59
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0} 	//channels 60..63
					#endif
				}
		},

		// pattern 5
		{
				.color =
				{
					#if LED_GROUP_PRESENT_0
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 0..3
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 4..7
					#endif
					#if LED_GROUP_PRESENT_1
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 8..11
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 12..15
					#endif
					#if LED_GROUP_PRESENT_2
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 16..19
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 20..23
					#endif
					#if LED_GROUP_PRESENT_3
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 24..27
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 28..31
					#endif
					#if LED_GROUP_PRESENT_4
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 32..35
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 36..39
					#endif
					#if LED_GROUP_PRESENT_5
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 40..43
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 44..47
					#endif
					#if LED_GROUP_PRESENT_6
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 48..51
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 52..55
					#endif
					#if LED_GROUP_PRESENT_7
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 56..59
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0} 	//channels 60..63
					#endif
				}
		},

		// pattern 6
		{
				.color =
				{
					#if LED_GROUP_PRESENT_0
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 0..3
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 4..7
					#endif
					#if LED_GROUP_PRESENT_1
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 8..11
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 12..15
					#endif
					#if LED_GROUP_PRESENT_2
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 16..19
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 20..23
					#endif
					#if LED_GROUP_PRESENT_3
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 24..27
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 28..31
					#endif
					#if LED_GROUP_PRESENT_4
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 32..35
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 36..39
					#endif
					#if LED_GROUP_PRESENT_5
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 40..43
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 44..47
					#endif
					#if LED_GROUP_PRESENT_6
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 48..51
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 52..55
					#endif
					#if LED_GROUP_PRESENT_7
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 56..59
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0} 	//channels 60..63
					#endif
				}
		},

		// pattern 7
		{
				.color =
				{
					#if LED_GROUP_PRESENT_0
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 0..3
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 4..7
					#endif
					#if LED_GROUP_PRESENT_1
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 8..11
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 12..15
					#endif
					#if LED_GROUP_PRESENT_2
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 16..19
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 20..23
					#endif
					#if LED_GROUP_PRESENT_3
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 24..27
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 28..31
					#endif
					#if LED_GROUP_PRESENT_4
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 32..35
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 36..39
					#endif
					#if LED_GROUP_PRESENT_5
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 40..43
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 44..47
					#endif
					#if LED_GROUP_PRESENT_6
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 48..51
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 52..55
					#endif
					#if LED_GROUP_PRESENT_7
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 56..59
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0} 	//channels 60..63
					#endif
				}
		},

		// pattern 8
		{
				.color =
				{
					#if LED_GROUP_PRESENT_0
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 0..3
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 4..7
					#endif
					#if LED_GROUP_PRESENT_1
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 8..11
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 12..15
					#endif
					#if LED_GROUP_PRESENT_2
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 16..19
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 20..23
					#endif
					#if LED_GROUP_PRESENT_3
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 24..27
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 28..31
					#endif
					#if LED_GROUP_PRESENT_4
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 32..35
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 36..39
					#endif
					#if LED_GROUP_PRESENT_5
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 40..43
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 44..47
					#endif
					#if LED_GROUP_PRESENT_6
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 48..51
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 52..55
					#endif
					#if LED_GROUP_PRESENT_7
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 56..59
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0} 	//channels 60..63
					#endif
				}
		},

		// pattern 9
		{
				.color =
				{
					#if LED_GROUP_PRESENT_0
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 0..3
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 4..7
					#endif
					#if LED_GROUP_PRESENT_1
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 8..11
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 12..15
					#endif
					#if LED_GROUP_PRESENT_2
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 16..19
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 20..23
					#endif
					#if LED_GROUP_PRESENT_3
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 24..27
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 28..31
					#endif
					#if LED_GROUP_PRESENT_4
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 32..35
					{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	{  0,   0,   0},	//channels 36..39
					#endif
					#if LED_GROUP_PRESENT_5
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 40..43
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 44..47
					#endif
					#if LED_GROUP_PRESENT_6
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 48..51
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 52..55
					#endif
					#if LED_GROUP_PRESENT_7
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0},	//channels 56..59
					{  0,   0,   0},	{  0,   0,   0},    {  0,   0,   0},    {  0,   0,   0} 	//channels 60..63
					#endif
				}
		}
};


#if LED_GROUP_PRESENT_0 ||  \
    LED_GROUP_PRESENT_1 ||  \
    LED_GROUP_PRESENT_2 ||  \
    LED_GROUP_PRESENT_3 ||  \
    LED_GROUP_PRESENT_4 ||  \
    LED_GROUP_PRESENT_5 ||  \
    LED_GROUP_PRESENT_6 ||  \
    LED_GROUP_PRESENT_7

#if defined(LEDS_VERSION_1)  // green pcb
static leds_definition_t leds[LEDS_COUNT] =
{
	#if LED_GROUP_PRESENT_0
	{.device_addr = &lp5024_0_addr, .channel = 7, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 0
	{.device_addr = &lp5024_0_addr, .channel = 6, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 1
	{.device_addr = &lp5024_0_addr, .channel = 5, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 2
	{.device_addr = &lp5024_0_addr, .channel = 4, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 3
	{.device_addr = &lp5024_0_addr, .channel = 3, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 4
	{.device_addr = &lp5024_0_addr, .channel = 2, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 5
	{.device_addr = &lp5024_0_addr, .channel = 1, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 6
	{.device_addr = &lp5024_0_addr, .channel = 0, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 7
	#endif
	#if LED_GROUP_PRESENT_1
	{.device_addr = &lp5024_1_addr, .channel = 7, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 8
	{.device_addr = &lp5024_1_addr, .channel = 6, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 9
	{.device_addr = &lp5024_1_addr, .channel = 5, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 10
	{.device_addr = &lp5024_1_addr, .channel = 4, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 11
	{.device_addr = &lp5024_1_addr, .channel = 3, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 12
	{.device_addr = &lp5024_1_addr, .channel = 2, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 13
	{.device_addr = &lp5024_1_addr, .channel = 1, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 14
	{.device_addr = &lp5024_1_addr, .channel = 0, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 15
	#endif
	#if LED_GROUP_PRESENT_2
	{.device_addr = &lp5024_2_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 16
	{.device_addr = &lp5024_2_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 17
	{.device_addr = &lp5024_2_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 18
	{.device_addr = &lp5024_2_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 19
	{.device_addr = &lp5024_2_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 20
	{.device_addr = &lp5024_2_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 21
	{.device_addr = &lp5024_2_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 22
	{.device_addr = &lp5024_2_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 23
	#endif
	#if LED_GROUP_PRESENT_3
	{.device_addr = &lp5024_3_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 24
	{.device_addr = &lp5024_3_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 25
	{.device_addr = &lp5024_3_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 26
	{.device_addr = &lp5024_3_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 27
	{.device_addr = &lp5024_3_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 28
	{.device_addr = &lp5024_3_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 29
	{.device_addr = &lp5024_3_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 30
	{.device_addr = &lp5024_3_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 31
	#endif
	#if LED_GROUP_PRESENT_4
	{.device_addr = &lp5024_4_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 32
	{.device_addr = &lp5024_4_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 33
	{.device_addr = &lp5024_4_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 34
	{.device_addr = &lp5024_4_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 35
	{.device_addr = &lp5024_4_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 36
	{.device_addr = &lp5024_4_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 37
	{.device_addr = &lp5024_4_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 38
	{.device_addr = &lp5024_4_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 39
	#endif
	#if LED_GROUP_PRESENT_5
	{.device_addr = &lp5024_5_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 40
	{.device_addr = &lp5024_5_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 41
	{.device_addr = &lp5024_5_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 42
	{.device_addr = &lp5024_5_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 43
	{.device_addr = &lp5024_5_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 44
	{.device_addr = &lp5024_5_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 45
	{.device_addr = &lp5024_5_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 46
	{.device_addr = &lp5024_5_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 47
	#endif
	#if LED_GROUP_PRESENT_6
	{.device_addr = &lp5024_6_addr, .channel = 7, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 48
	{.device_addr = &lp5024_6_addr, .channel = 6, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 49
	{.device_addr = &lp5024_6_addr, .channel = 5, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 50
	{.device_addr = &lp5024_6_addr, .channel = 4, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 51
	{.device_addr = &lp5024_6_addr, .channel = 3, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 52
	{.device_addr = &lp5024_6_addr, .channel = 2, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 53
	{.device_addr = &lp5024_6_addr, .channel = 1, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 54
	{.device_addr = &lp5024_6_addr, .channel = 0, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 55
	#endif
	#if LED_GROUP_PRESENT_7
	{.device_addr = &lp5024_7_addr, .channel = 7, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 56
	{.device_addr = &lp5024_7_addr, .channel = 6, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 57
	{.device_addr = &lp5024_7_addr, .channel = 5, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 58
	{.device_addr = &lp5024_7_addr, .channel = 4, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 59
	{.device_addr = &lp5024_7_addr, .channel = 3, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 60
	{.device_addr = &lp5024_7_addr, .channel = 2, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 61
	{.device_addr = &lp5024_7_addr, .channel = 1, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 62
	{.device_addr = &lp5024_7_addr, .channel = 0, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 63
	#endif
};
#endif
#if defined(LEDS_VERSION_2)  //bigger
static leds_definition_t leds[LEDS_COUNT] =
{
	#if LED_GROUP_PRESENT_0
	{.device_addr = &lp5024_0_addr, .channel = 7, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 0
	{.device_addr = &lp5024_0_addr, .channel = 6, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 1
	{.device_addr = &lp5024_0_addr, .channel = 5, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 2
	{.device_addr = &lp5024_0_addr, .channel = 4, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 3
	{.device_addr = &lp5024_0_addr, .channel = 3, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 4
	{.device_addr = &lp5024_0_addr, .channel = 2, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 5
	{.device_addr = &lp5024_0_addr, .channel = 1, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 6
	{.device_addr = &lp5024_0_addr, .channel = 0, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 7
	#endif
	#if LED_GROUP_PRESENT_1
	{.device_addr = &lp5024_1_addr, .channel = 7, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 8
	{.device_addr = &lp5024_1_addr, .channel = 6, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 9
	{.device_addr = &lp5024_1_addr, .channel = 5, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 10
	{.device_addr = &lp5024_1_addr, .channel = 4, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 11
	{.device_addr = &lp5024_1_addr, .channel = 3, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 12
	{.device_addr = &lp5024_1_addr, .channel = 2, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 13
	{.device_addr = &lp5024_1_addr, .channel = 1, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 14
	{.device_addr = &lp5024_1_addr, .channel = 0, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 15
	#endif
	#if LED_GROUP_PRESENT_2
	{.device_addr = &lp5024_2_addr, .channel = 7, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 16
	{.device_addr = &lp5024_2_addr, .channel = 6, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 17
	{.device_addr = &lp5024_2_addr, .channel = 5, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 18
	{.device_addr = &lp5024_2_addr, .channel = 4, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 19
	{.device_addr = &lp5024_2_addr, .channel = 3, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 20
	{.device_addr = &lp5024_2_addr, .channel = 2, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 21
	{.device_addr = &lp5024_2_addr, .channel = 1, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 22
	{.device_addr = &lp5024_2_addr, .channel = 0, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 23
	#endif
	#if LED_GROUP_PRESENT_3
	{.device_addr = &lp5024_3_addr, .channel = 7, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 24
	{.device_addr = &lp5024_3_addr, .channel = 6, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 25
	{.device_addr = &lp5024_3_addr, .channel = 5, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 26
	{.device_addr = &lp5024_3_addr, .channel = 4, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 27
	{.device_addr = &lp5024_3_addr, .channel = 3, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 28
	{.device_addr = &lp5024_3_addr, .channel = 2, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 29
	{.device_addr = &lp5024_3_addr, .channel = 1, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 30
	{.device_addr = &lp5024_3_addr, .channel = 0, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 31
	#endif
	#if LED_GROUP_PRESENT_4
	{.device_addr = &lp5024_4_addr, .channel = 7, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 32
	{.device_addr = &lp5024_4_addr, .channel = 6, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 33
	{.device_addr = &lp5024_4_addr, .channel = 5, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 34
	{.device_addr = &lp5024_4_addr, .channel = 4, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 35
	{.device_addr = &lp5024_4_addr, .channel = 3, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 36
	{.device_addr = &lp5024_4_addr, .channel = 2, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 37
	{.device_addr = &lp5024_4_addr, .channel = 1, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 38
	{.device_addr = &lp5024_4_addr, .channel = 0, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 39
	#endif
	#if LED_GROUP_PRESENT_5
	{.device_addr = &lp5024_5_addr, .channel = 7, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 40
	{.device_addr = &lp5024_5_addr, .channel = 6, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 41
	{.device_addr = &lp5024_5_addr, .channel = 5, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 42
	{.device_addr = &lp5024_5_addr, .channel = 4, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 43
	{.device_addr = &lp5024_5_addr, .channel = 3, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 44
	{.device_addr = &lp5024_5_addr, .channel = 2, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 45
	{.device_addr = &lp5024_5_addr, .channel = 1, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 46
	{.device_addr = &lp5024_5_addr, .channel = 0, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 47
	#endif
	#if LED_GROUP_PRESENT_6
	{.device_addr = &lp5024_6_addr, .channel = 7, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 48
	{.device_addr = &lp5024_6_addr, .channel = 6, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 49
	{.device_addr = &lp5024_6_addr, .channel = 5, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 50
	{.device_addr = &lp5024_6_addr, .channel = 4, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 51
	{.device_addr = &lp5024_6_addr, .channel = 3, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 52
	{.device_addr = &lp5024_6_addr, .channel = 2, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 53
	{.device_addr = &lp5024_6_addr, .channel = 1, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 54
	{.device_addr = &lp5024_6_addr, .channel = 0, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 55
	#endif
	#if LED_GROUP_PRESENT_7
	{.device_addr = &lp5024_7_addr, .channel = 7, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 56
	{.device_addr = &lp5024_7_addr, .channel = 6, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 57
	{.device_addr = &lp5024_7_addr, .channel = 5, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 58
	{.device_addr = &lp5024_7_addr, .channel = 4, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 59
	{.device_addr = &lp5024_7_addr, .channel = 3, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 60
	{.device_addr = &lp5024_7_addr, .channel = 2, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 61
	{.device_addr = &lp5024_7_addr, .channel = 1, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 62
	{.device_addr = &lp5024_7_addr, .channel = 0, .rgb_map = {.r_ch=2, .g_ch=1, .b_ch=0 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 63
	#endif
};
#endif
#if defined(LEDS_VERSION_3) //small leds
static leds_definition_t leds[LEDS_COUNT] =
{
	#if LED_GROUP_PRESENT_0
	{.device_addr = &lp5024_0_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 0
	{.device_addr = &lp5024_0_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 1
	{.device_addr = &lp5024_0_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 2
	{.device_addr = &lp5024_0_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 3
	{.device_addr = &lp5024_0_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 4
	{.device_addr = &lp5024_0_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 5
	{.device_addr = &lp5024_0_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 6
	{.device_addr = &lp5024_0_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 7
	#endif
	#if LED_GROUP_PRESENT_1
	{.device_addr = &lp5024_1_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 8
	{.device_addr = &lp5024_1_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 9
	{.device_addr = &lp5024_1_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 10
	{.device_addr = &lp5024_1_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 11
	{.device_addr = &lp5024_1_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 12
	{.device_addr = &lp5024_1_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 13
	{.device_addr = &lp5024_1_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 14
	{.device_addr = &lp5024_1_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 15
	#endif
	#if LED_GROUP_PRESENT_2
	{.device_addr = &lp5024_2_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 16
	{.device_addr = &lp5024_2_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 17
	{.device_addr = &lp5024_2_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 18
	{.device_addr = &lp5024_2_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 19
	{.device_addr = &lp5024_2_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 20
	{.device_addr = &lp5024_2_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 21
	{.device_addr = &lp5024_2_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 22
	{.device_addr = &lp5024_2_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 23
	#endif
	#if LED_GROUP_PRESENT_3
	{.device_addr = &lp5024_3_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 24
	{.device_addr = &lp5024_3_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 25
	{.device_addr = &lp5024_3_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 26
	{.device_addr = &lp5024_3_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 27
	{.device_addr = &lp5024_3_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 28
	{.device_addr = &lp5024_3_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 29
	{.device_addr = &lp5024_3_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 30
	{.device_addr = &lp5024_3_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 31
	#endif
	#if LED_GROUP_PRESENT_4
	{.device_addr = &lp5024_4_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 32
	{.device_addr = &lp5024_4_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 33
	{.device_addr = &lp5024_4_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 34
	{.device_addr = &lp5024_4_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 35
	{.device_addr = &lp5024_4_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 36
	{.device_addr = &lp5024_4_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 37
	{.device_addr = &lp5024_4_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 38
	{.device_addr = &lp5024_4_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 39
	#endif
	#if LED_GROUP_PRESENT_5
	{.device_addr = &lp5024_5_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 40
	{.device_addr = &lp5024_5_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 41
	{.device_addr = &lp5024_5_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 42
	{.device_addr = &lp5024_5_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 43
	{.device_addr = &lp5024_5_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 44
	{.device_addr = &lp5024_5_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 45
	{.device_addr = &lp5024_5_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 46
	{.device_addr = &lp5024_5_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 47
	#endif
	#if LED_GROUP_PRESENT_6
	{.device_addr = &lp5024_6_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 48
	{.device_addr = &lp5024_6_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 49
	{.device_addr = &lp5024_6_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 50
	{.device_addr = &lp5024_6_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 51
	{.device_addr = &lp5024_6_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 52
	{.device_addr = &lp5024_6_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 53
	{.device_addr = &lp5024_6_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 54
	{.device_addr = &lp5024_6_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 55
	#endif
	#if LED_GROUP_PRESENT_7
	{.device_addr = &lp5024_7_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 56
	{.device_addr = &lp5024_7_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 57
	{.device_addr = &lp5024_7_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 58
	{.device_addr = &lp5024_7_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 59
	{.device_addr = &lp5024_7_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 60
	{.device_addr = &lp5024_7_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 61
	{.device_addr = &lp5024_7_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 62
	{.device_addr = &lp5024_7_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 63
	#endif
};
#endif
#if defined(LEDS_VERSION_4) //test board small
static leds_definition_t leds[LEDS_COUNT] =
{
	#if LED_GROUP_PRESENT_0
	{.device_addr = &lp5024_0_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 0
	{.device_addr = &lp5024_0_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 1
	{.device_addr = &lp5024_0_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 2
	{.device_addr = &lp5024_0_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 3
	{.device_addr = &lp5024_0_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 4
	{.device_addr = &lp5024_0_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 5
	{.device_addr = &lp5024_0_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 6
	{.device_addr = &lp5024_0_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 7
	#endif
	#if LED_GROUP_PRESENT_1
	{.device_addr = &lp5024_1_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 8
	{.device_addr = &lp5024_1_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 9
	{.device_addr = &lp5024_1_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 10
	{.device_addr = &lp5024_1_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 11
	{.device_addr = &lp5024_1_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 12
	{.device_addr = &lp5024_1_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 13
	{.device_addr = &lp5024_1_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 14
	{.device_addr = &lp5024_1_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 15
	#endif
	#if LED_GROUP_PRESENT_2
	{.device_addr = &lp5024_2_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 16
	{.device_addr = &lp5024_2_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 17
	{.device_addr = &lp5024_2_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 18
	{.device_addr = &lp5024_2_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 19
	{.device_addr = &lp5024_2_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 20
	{.device_addr = &lp5024_2_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 21
	{.device_addr = &lp5024_2_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 22
	{.device_addr = &lp5024_2_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 23
	#endif
	#if LED_GROUP_PRESENT_3
	{.device_addr = &lp5024_3_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 24
	{.device_addr = &lp5024_3_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 25
	{.device_addr = &lp5024_3_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 26
	{.device_addr = &lp5024_3_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 27
	{.device_addr = &lp5024_3_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 28
	{.device_addr = &lp5024_3_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 29
	{.device_addr = &lp5024_3_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 30
	{.device_addr = &lp5024_3_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 31
	#endif
	#if LED_GROUP_PRESENT_4
	{.device_addr = &lp5024_4_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 32
	{.device_addr = &lp5024_4_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 33
	{.device_addr = &lp5024_4_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 34
	{.device_addr = &lp5024_4_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 35
	{.device_addr = &lp5024_4_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 36
	{.device_addr = &lp5024_4_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 37
	{.device_addr = &lp5024_4_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 38
	{.device_addr = &lp5024_4_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 39
	#endif
	#if LED_GROUP_PRESENT_5
	{.device_addr = &lp5024_5_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 40
	{.device_addr = &lp5024_5_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 41
	{.device_addr = &lp5024_5_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 42
	{.device_addr = &lp5024_5_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 43
	{.device_addr = &lp5024_5_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 44
	{.device_addr = &lp5024_5_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 45
	{.device_addr = &lp5024_5_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 46
	{.device_addr = &lp5024_5_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 47
	#endif
	#if LED_GROUP_PRESENT_6
	{.device_addr = &lp5024_6_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 48
	{.device_addr = &lp5024_6_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 49
	{.device_addr = &lp5024_6_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 50
	{.device_addr = &lp5024_6_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 51
	{.device_addr = &lp5024_6_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 52
	{.device_addr = &lp5024_6_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 53
	{.device_addr = &lp5024_6_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 54
	{.device_addr = &lp5024_6_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 55
	#endif
	#if LED_GROUP_PRESENT_7
	{.device_addr = &lp5024_7_addr, .channel = 7, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 56
	{.device_addr = &lp5024_7_addr, .channel = 6, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 57
	{.device_addr = &lp5024_7_addr, .channel = 5, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 58
	{.device_addr = &lp5024_7_addr, .channel = 4, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 59
	{.device_addr = &lp5024_7_addr, .channel = 3, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 60
	{.device_addr = &lp5024_7_addr, .channel = 2, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 61
	{.device_addr = &lp5024_7_addr, .channel = 1, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 62
	{.device_addr = &lp5024_7_addr, .channel = 0, .rgb_map = {.r_ch=0, .g_ch=1, .b_ch=2 }, .color = {.r=0, .g=0, .b=0}, .priority = 0 },	// 63
	#endif
};
#endif

#endif //LED_GROUP_PRESENT


void led_init(float gamma_r, float gamma_g, float gamma_b)
{
	#if LED_GROUP_PRESENT_0 ||  \
		LED_GROUP_PRESENT_1 ||  \
		LED_GROUP_PRESENT_2 ||  \
		LED_GROUP_PRESENT_3 ||  \
		LED_GROUP_PRESENT_4 ||  \
		LED_GROUP_PRESENT_5 ||  \
		LED_GROUP_PRESENT_6 ||  \
		LED_GROUP_PRESENT_7

		HAL_GPIO_WritePin(OUT_LED_ENABLE_GPIO_Port, OUT_LED_ENABLE_Pin, GPIO_PIN_SET);
	#endif

	#if LED_GROUP_PRESENT_0
		lp5024_Init(lp5024_0_addr);
	#endif
	#if LED_GROUP_PRESENT_1
		lp5024_Init(lp5024_1_addr);
	#endif
	#if LED_GROUP_PRESENT_2
		lp5024_Init(lp5024_2_addr);
	#endif
	#if LED_GROUP_PRESENT_3
		lp5024_Init(lp5024_3_addr);
	#endif
	#if LED_GROUP_PRESENT_4
		lp5024_Init(lp5024_4_addr);
	#endif
	#if LED_GROUP_PRESENT_5
		lp5024_Init(lp5024_5_addr);
	#endif
	#if LED_GROUP_PRESENT_6
		lp5024_Init(lp5024_6_addr);
	#endif
	#if LED_GROUP_PRESENT_7
		lp5024_Init(lp5024_7_addr);
	#endif

	calculate_lut_gamma(lut_gamma_r, gamma_r);
	calculate_lut_gamma(lut_gamma_g, gamma_g);
	calculate_lut_gamma(lut_gamma_b, gamma_b);
}

/*
static float pow_f(float base, float ex);

static float nth_root(float A, int n)
{
    //const int K = 6;
    float x[6] = {1.0,1.0,1.0,1.0,1.0,1.0};
    for (int k = 0; k < 6 - 1; k++)
    {
        x[k + 1] = (1.0 / n) * ((n - 1) * x[k] + A / pow_f(x[k], n - 1));
    }
    return x[6-1];
}

static float pow_f(float base, float ex)
{
    if (base == 0)
    {
        return 0;
    }
    // power of 0
    if (ex == 0)
    {
        return 1;
    // negative exponenet
    }
    else if( ex < 0)
    {
        return 1 / pow_f(base, -ex);
    // fractional exponent
    }
    else if (ex > 0 && ex < 1)
    {
        return nth_root(base, 1/ex);
    }
    else if ((int)ex % 2 == 0)
    {
        float half_pow = pow_f(base, ex/2);
        return half_pow * half_pow;
    //integer exponenet
    }
    else
    {
        return base * pow_f(base, ex - 1);
    }
}
*/

void calculate_lut_gamma(uint8_t *lut_table, float gamma_data)
{
	//float a1,a2,a3, b1, b2, b3;

	//a1 = 2.0;
	//b1 = 3.0;

	//a2 = 2.5;
	//b2 = 3.4;

	//a3 = 0.24;
	//b3= 0.69;


	//art_printf("t1 : %d\r\n",(int)(pow_f(a1, b1)*1000.0));
	//art_printf("t2 : %d\r\n",(int)(pow_f(a2, b2)*1000.0));
	//art_printf("t3 : %d\r\n",(int)(pow_f(a3, b3)*1000.0));
	//art_printf("t4 : %d\r\n",(int)(pow_f(2, 3)));

	for (uint32_t i=0; i<256; i++)
	{
		//art_printf("%d : %d   %d   %d\r\n",
		//		i,
		//		(int)(( (float)i / 255.0) * 1000.0),
		//		(int)((1.0 / gamma_data) * 1000.0),
		//		(int)(pow_f(((float)i / 255.0) , (1.0 / gamma_data))*1000.0));

		lut_table[i] = (uint8_t)( 255.0 * ( pow_f(((float)i / 255.0) , (1.0 / gamma_data)) ));
	}
}

void print_lut_gamma(uint8_t *lut_table)
{
	for (uint32_t i=0; i<256; i++)
	{
		art_printf("%d -> %d\r\n",i,lut_table[i]);
	}
}

uint8_t led_gamma(uint8_t *lut_table, uint8_t data)
{
	return lut_table[data];
}


#if LED_GROUP_PRESENT_0 ||  \
	LED_GROUP_PRESENT_1 ||  \
	LED_GROUP_PRESENT_2 ||  \
	LED_GROUP_PRESENT_3 ||  \
	LED_GROUP_PRESENT_4 ||  \
	LED_GROUP_PRESENT_5 ||  \
	LED_GROUP_PRESENT_6 ||  \
	LED_GROUP_PRESENT_7
static led_channel_value_t rgb_remap(led_color_t color, rgb_map_t rgb_map)
{
	led_channel_value_t value;

	value.intensity = 255;
	value.ch0 = 0;
	value.ch1 = 0;
	value.ch2 = 0;

	switch (rgb_map.r_ch)
	{
		case 0:  value.ch0 = color.r;  break;
		case 1:  value.ch1 = color.r;  break;
		case 2:  value.ch2 = color.r;  break;
		default: break;
	}

	switch (rgb_map.g_ch)
	{
		case 0:  value.ch0 = color.g;  break;
		case 1:  value.ch1 = color.g;  break;
		case 2:  value.ch2 = color.g;  break;
		default: break;
	}

	switch (rgb_map.b_ch)
	{
		case 0:  value.ch0 = color.b;  break;
		case 1:  value.ch1 = color.b;  break;
		case 2:  value.ch2 = color.b;  break;
		default: break;
	}

	return value;
}
#endif

bool led_set_color_pure(uint32_t i, led_color_t color, uint8_t priority)
{
	bool ret_val = false;

	#if LED_GROUP_PRESENT_0 ||  \
		LED_GROUP_PRESENT_1 ||  \
		LED_GROUP_PRESENT_2 ||  \
		LED_GROUP_PRESENT_3 ||  \
		LED_GROUP_PRESENT_4 ||  \
		LED_GROUP_PRESENT_5 ||  \
		LED_GROUP_PRESENT_6 ||  \
		LED_GROUP_PRESENT_7
	if (priority >= leds[i].priority)
	{
		if ((leds[i].color.r != color.r) ||
		    (leds[i].color.g != color.g) ||
		    (leds[i].color.b != color.b))
		{
			ret_val = true;

			leds[i].color.r = color.r;
			leds[i].color.g = color.g;
			leds[i].color.b = color.b;

			if ((color.r == 0) && (color.g == 0) && (color.b == 0))
			{
				leds[i].priority = 0;
			}
			else
			{
				leds[i].priority = priority;
			}
		}
	}
	#endif

	return ret_val;
}

bool led_set_color_gamma(uint32_t i, led_color_t color, uint8_t priority)
{
	color.r = led_gamma(lut_gamma_r, color.r);
	color.g = led_gamma(lut_gamma_g, color.g);
	color.b = led_gamma(lut_gamma_b, color.b);

	return led_set_color_pure(i, color, priority);
}


void led_drive(uint32_t i, ids_t id)
{
	#if LED_GROUP_PRESENT_0 ||  \
		LED_GROUP_PRESENT_1 ||  \
		LED_GROUP_PRESENT_2 ||  \
		LED_GROUP_PRESENT_3 ||  \
		LED_GROUP_PRESENT_4 ||  \
		LED_GROUP_PRESENT_5 ||  \
		LED_GROUP_PRESENT_6 ||  \
		LED_GROUP_PRESENT_7
		lp5024_LedOn(*(leds[i].device_addr), leds[i].channel, rgb_remap(leds[i].color, leds[i].rgb_map), id);
	#endif
}

void led_debug_set_red(void)
{
	HAL_GPIO_WritePin(OUT_LEDR_GPIO_Port, OUT_LEDR_Pin, GPIO_PIN_RESET);
}

void led_debug_clr_red(void)
{
	HAL_GPIO_WritePin(OUT_LEDR_GPIO_Port, OUT_LEDR_Pin, GPIO_PIN_SET);
}

void led_debug_set_green(void)
{
	HAL_GPIO_WritePin(OUT_LEDG_GPIO_Port, OUT_LEDG_Pin, GPIO_PIN_RESET);
}

void led_debug_clr_green(void)
{
	HAL_GPIO_WritePin(OUT_LEDG_GPIO_Port, OUT_LEDG_Pin, GPIO_PIN_SET);
}

//stage = 0..1540
void led_blink(uint32_t channel, uint32_t stage, uint8_t priority)
{
	// 0..255     R up
	// 256..512   R down
	// 513..768   G up
	// 769..1024  G down
	// 1025..1280 B up
	// 1281..1536 B down

#if LED_GROUP_PRESENT_0 ||  \
	LED_GROUP_PRESENT_1 ||  \
	LED_GROUP_PRESENT_2 ||  \
	LED_GROUP_PRESENT_3 ||  \
	LED_GROUP_PRESENT_4 ||  \
	LED_GROUP_PRESENT_5 ||  \
	LED_GROUP_PRESENT_6 ||  \
	LED_GROUP_PRESENT_7
	if (priority >= leds[channel].priority)
	{

		if ((stage>0) && (stage<256))
		{
			leds[channel].color.r++;
			leds[channel].color.g = 0;
			leds[channel].color.b = 0;
		}
		else if ((stage>=256) && (stage<513))
		{
			leds[channel].color.r--;
			leds[channel].color.g = 0;
			leds[channel].color.b = 0;
		}
		else if ((stage>=513) && (stage<769))
		{
			leds[channel].color.r = 0;
			leds[channel].color.g++;
			leds[channel].color.b = 0;
		}
		else if ((stage>=769) && (stage<1025))
		{
			leds[channel].color.r = 0;
			leds[channel].color.g--;
			leds[channel].color.b = 0;
		}
		else if ((stage>=1025) && (stage<1281))
		{
			leds[channel].color.r = 0;
			leds[channel].color.g = 0;
			leds[channel].color.b++;
		}
		else if ((stage>=1281) && (stage<1537))
		{
			leds[channel].color.r = 0;
			leds[channel].color.g = 0;
			leds[channel].color.b--;
		}
		else
		{
			leds[channel].color.r = 0;
			leds[channel].color.g = 0;
			leds[channel].color.b = 0;
		}

		if ((leds[channel].color.r == 0) && (leds[channel].color.g == 0) && (leds[channel].color.b == 0))
		{
			leds[channel].priority = 0;
		}
		else
		{
			leds[channel].priority = priority;
		}
	}
#endif
}

uint32_t change_ch(uint32_t ch, int32_t offset)
{
	int32_t c;

	c = (int32_t)ch+offset;

	if (c<0)
	{
		c=LEDS_COUNT-1;
	}
	else if (c>(LEDS_COUNT-1))
	{
		c=0;
	}



	return (uint32_t)c;
}

void pwm_led_init(uint8_t led_pwm_nmb, TIM_HandleTypeDef *tim_handle_ptr, uint16_t channel)
{
	if (led_pwm_nmb < PWM_CHANNELS)
	{
		tim_def[led_pwm_nmb].handle_ptr  = tim_handle_ptr;
		tim_def[led_pwm_nmb].pwm_channel = channel;
	}
}

void pwm_led_set(uint8_t led_pwm_nmb, uint16_t value)
{
	if (led_pwm_nmb < PWM_CHANNELS)
	{
	    TIM_OC_InitTypeDef sConfigOC;

	    sConfigOC.OCMode = TIM_OCMODE_PWM1;
	    sConfigOC.Pulse = value;
	    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	    HAL_TIM_PWM_ConfigChannel(tim_def[led_pwm_nmb].handle_ptr, &sConfigOC, tim_def[led_pwm_nmb].pwm_channel);
	    HAL_TIM_PWM_Start(tim_def[led_pwm_nmb].handle_ptr, tim_def[led_pwm_nmb].pwm_channel);
	}
}

void led_set_color_and_drive(const pattern_t *pattern, uint8_t priority)
{
	if (pattern != NULL)
	{
		for (uint32_t i=0; i<LEDS_COUNT; i++)
		{
			if (led_set_color_gamma(i, pattern->color[i], priority) == true)
			{
				// drive led only if necessary
				led_drive(i, LED_SCENE_ID);
			}
		}
	}
}

void scenes_init(void)
{
	active_scene_number = 0;
	play_scene_indicator = false;
}

uint8_t get_active_scene_number(void)
{
	return active_scene_number;
}

void set_active_scene_number(uint8_t data)
{
	if (data < SCENES_COUNT)
	{
		active_scene_number = data;
	}
}

void play_scene(void)
{
	play_scene_indicator = true;
}

void stop_scene(void)
{
	play_scene_indicator = false;
}

bool is_play_scene(void)
{
	return play_scene_indicator;
}

void set_base_frequency(uint32_t data)
{
	// in [ms]
	base_frequency = data;
}

static uint32_t get_base_frequency(void)
{
	// ms per tick
	return base_frequency;
}

static err_code_t get_active_scene_elements(uint8_t scene_number, uint8_t *active_elements)
{
	if (scene_number >= SCENES_COUNT)
	{
		art_printf("[%s] (%d) \r\n",__func__, __LINE__);
		return RESULT_BAD_PARAMETER;
	}

	*active_elements = scenes[scene_number].active_scene_elements;

	return RESULT_OK;
}

err_code_t get_scene_pattern_info(uint8_t scene_number, uint8_t pattern_number, pattern_t *pattern, uint16_t *duration)
{
	if (scene_number >= SCENES_COUNT)
	{
		art_printf("[%s] (%d) \r\n",__func__, __LINE__);
		return RESULT_BAD_PARAMETER;
	}

	if (pattern_number >= scenes[scene_number].active_scene_elements)
	{
		art_printf("[%s] (%d) \r\n",__func__, __LINE__);
		return RESULT_BAD_PARAMETER;
	}

	if (pattern_number >= SCENE_PATTERN_COUNT)
	{
		art_printf("[%s] (%d) \r\n",__func__, __LINE__);
		return RESULT_BAD_PARAMETER;
	}

	if (pattern == NULL)
	{
		art_printf("[%s] (%d) \r\n",__func__, __LINE__);
		return RESULT_BAD_PARAMETER;
	}

	uint8_t requested_pattern_number = scenes[scene_number].scene_elements[pattern_number].pattern_number;
	for (uint32_t i=0; i<LEDS_COUNT; i++)
	{
		//TODO: sprawdzic, czy to sie przekopiuje
		pattern->color[i] = led_patterns[requested_pattern_number].color[i];
	}
	*duration = scenes[scene_number].scene_elements[scene_number].duration;

	return RESULT_OK;
}

static err_code_t calculate_new_pattern(pattern_t *new_pattern,
		                         pattern_t *current_pattern,
						         pattern_t *next_pattern,
						         uint32_t current_step_ms,
						         uint32_t current_duration_ms)
{
	if ((new_pattern == NULL) ||
	    (current_pattern == NULL) ||
		(next_pattern == NULL) ||
		(current_duration_ms == 0))
	{
		art_printf("[%s] (%d) \r\n",__func__, __LINE__);
		return RESULT_BAD_PARAMETER;
	}

	for (uint32_t i=0; i<LEDS_COUNT; i++)
	{
		new_pattern->color[i].r = (uint8_t)(((((int32_t)next_pattern->color[i].r - (int32_t)current_pattern->color[i].r) * (int32_t)current_step_ms) / (int32_t)current_duration_ms) + (int32_t)current_pattern->color[i].r);
		new_pattern->color[i].g = (uint8_t)(((((int32_t)next_pattern->color[i].g - (int32_t)current_pattern->color[i].g) * (int32_t)current_step_ms) / (int32_t)current_duration_ms) + (int32_t)current_pattern->color[i].g);
		new_pattern->color[i].b = (uint8_t)(((((int32_t)next_pattern->color[i].b - (int32_t)current_pattern->color[i].b) * (int32_t)current_step_ms) / (int32_t)current_duration_ms) + (int32_t)current_pattern->color[i].b);
	}

	return RESULT_OK;
}

static err_code_t correct_intensty_of_pattern(pattern_t *new_pattern, uint16_t light)
{
	if (new_pattern == NULL)
	{
		art_printf("[%s] (%d) \r\n",__func__, __LINE__);
		return RESULT_BAD_PARAMETER;
	}

	float factor = ((float)light / 500.0);
	if (factor > 1.0)
	{
		factor = 1.0;
	}

	uint8_t fdiv = (uint8_t)(1.0 / factor);

	for (uint32_t i=0; i<LEDS_COUNT; i++)
	{
		new_pattern->color[i].r = new_pattern->color[i].r / fdiv;
		new_pattern->color[i].g = new_pattern->color[i].g / fdiv;
		new_pattern->color[i].b = new_pattern->color[i].b / fdiv;
	}

	return RESULT_OK;
}

static err_code_t correct_chrominance_of_pattern(pattern_t *new_pattern, float temp)
{
	if (new_pattern == NULL)
	{
		art_printf("[%s] (%d) \r\n",__func__, __LINE__);
		return RESULT_BAD_PARAMETER;
	}

	float factor_r = 1.0;
	float factor_b = 1.0;

	if (temp > 0.0)
	{
	    factor_r = temp / 20.0;
	}
	else
	{
	    factor_b = ((-1.0)*temp) / 10.0;
	    factor_r = 1 / factor_b;
	}


	for (uint32_t i=0; i<LEDS_COUNT; i++)
	{
		new_pattern->color[i].r = (float)new_pattern->color[i].r * factor_r;
		new_pattern->color[i].b = (float)new_pattern->color[i].b * factor_b;
	}

	return RESULT_OK;
}

void execute_active_scene(float temperature, uint16_t light)
{
	static uint8_t current_pattern_number = 0;
	static uint8_t next_pattern_number = 1;
	uint16_t current_duration;
	uint16_t next_duration;
	pattern_t current_pattern;
	pattern_t next_pattern;
	pattern_t new_pattern;
	uint8_t scene_elements;
	static uint32_t current_step_ms = 0;
	uint32_t current_duration_ms;

	//art_printf(".\r\n");

	if ( (get_active_scene_elements( get_active_scene_number(), &scene_elements) == RESULT_OK) &&
		 (get_scene_pattern_info( get_active_scene_number(), current_pattern_number, &current_pattern, &current_duration) == RESULT_OK) &&
	     (get_scene_pattern_info( get_active_scene_number(), next_pattern_number,    &next_pattern,    &next_duration) == RESULT_OK) )
	{
		current_duration_ms = (uint32_t)current_duration * 100;

		calculate_new_pattern(&new_pattern, &current_pattern, &next_pattern, current_step_ms, current_duration_ms);

/*
		art_printf("s=%d, d=%d, new: %d %d %d, current: %d %d %d  next: %d %d %d \r\n",
				current_step_ms,
				current_duration_ms,
				new_pattern.color[0].r,     new_pattern.color[0].g,     new_pattern.color[0].b,
				current_pattern.color[0].r, current_pattern.color[0].g, current_pattern.color[0].b,
				next_pattern.color[0].r,    next_pattern.color[0].g,    next_pattern.color[0].b);
*/

		correct_intensty_of_pattern(&new_pattern, light);
		correct_chrominance_of_pattern(&new_pattern, temperature);

		led_set_color_and_drive(&new_pattern, LED_PRIORITY_SMOOTH);


		current_step_ms += (uint32_t)get_base_frequency();
		if (current_step_ms >= current_duration_ms)
		{
			current_step_ms = 0;

			current_pattern_number++;
			if (current_pattern_number >= scene_elements)
			{
				current_pattern_number = 0;
			}

			next_pattern_number++;
			if (next_pattern_number >= scene_elements)
			{
				next_pattern_number = 0;
			}
		}
	}
}



static float temperature = 0.0;
static bool update_temperature = false;

bool is_update_temperature(void)
{
	return update_temperature;
}

void update_temperature_value(float temp)
{
	temperature = temp;
	update_temperature = true;
}

float get_temperature_value(void)
{
	return temperature;
}

void execute_update_temperature(void)
{
	led_color_t color = {0, 0, 0};

	if (temperature > 0.0)
	{
	    if (temperature < 25.0)
	    {
		    color.r = (temperature * 10.0);
	    }
	    else
	    {
		    color.r = 255;
	    }
	    color.b = 0;
	}
	else
	{
	    color.r = 0;
	    if (temperature > (-25.0))
	    {
		    color.b = (temperature * 10.0) * (-1.0);
	    }
	    else
	    {
		    color.b = 255;
	    }
	}

	led_set_color_gamma(23, color, LED_PRIORITY_TEMPERATURE);
	led_set_color_gamma(24, color, LED_PRIORITY_TEMPERATURE);
	led_set_color_gamma(25, color, LED_PRIORITY_TEMPERATURE);
	led_drive(23, LED_TEMP_ID);
	led_drive(24, LED_TEMP_ID);
	led_drive(25, LED_TEMP_ID);

	update_temperature = false;
}



static uint16_t light = 0.0;
static bool update_light = false;

bool is_update_light(void)
{
	return update_light;
}


void update_light_value(uint16_t lig)
{
	light = lig;
	update_light = true;
}

uint16_t get_light_value(void)
{
	return light;
}

void execute_update_light(void)
{
	led_color_t color = {0, 0, 0};

	if ((light / 25) > 255)
	{
	    color.g = 255;
	}
	else
	{
	    color.g = light / 25;
	}

	led_set_color_gamma(38, color, LED_PRIORITY_LIGHT);
	led_set_color_gamma(39, color, LED_PRIORITY_LIGHT);
	led_set_color_gamma(40, color, LED_PRIORITY_LIGHT);
	led_drive(38, LED_LIGHT_ID);
	led_drive(39, LED_LIGHT_ID);
	led_drive(40, LED_LIGHT_ID);

	update_light = false;
}













