/*
 * errors.h
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */

#ifndef INC_ERRORS_H_
#define INC_ERRORS_H_


typedef enum
{
	RESULT_OK            = 0,
	RESULT_ERROR         = 1,
	RESULT_BUSY          = 2,
	RESULT_BAD_PARAMETER = 3,
	RESULT_NO_MEMORY     = 4,
	RESULT_IDLE          = 5,
	RESULT_ERROR_LAST    = 6

}err_code_t;


#endif /* INC_ERRORS_H_ */
