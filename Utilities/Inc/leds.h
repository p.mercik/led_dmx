/*
 * leds.h
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */

#ifndef LEDS_H_
#define LEDS_H_

#include <stdbool.h>
#include "main.h"
#include "errors.h"
#include "ic_devices.h"



#define LEDS_PATTERNS_COUNT        10
#define SCENE_PATTERN_COUNT        4
#define SCENES_COUNT               4

#define LED_PRIORITY_GEST          10
#define LED_PRIORITY_TOUCH         8
#define LED_PRIORITY_TEMPERATURE   7
#define LED_PRIORITY_LIGHT         6
#define LED_PRIORITY_ACC           5
#define LED_PRIORITY_BLINK         3
#define LED_PRIORITY_SMOOTH        1


#define LED_PWM_1    0
#define LED_PWM_2    1


typedef struct
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
}led_color_t;

typedef struct
{
	led_color_t color[LEDS_COUNT];
}pattern_t;







void led_init(float gamma_r, float gamma_g, float gamma_b);

void calculate_lut_gamma(uint8_t *lut_table, float gamma_data);
void print_lut_gamma(uint8_t *lut_table);
uint8_t led_gamma(uint8_t *lut_table, uint8_t data);

bool led_set_color_pure(uint32_t i, led_color_t color, uint8_t priority);
bool led_set_color_gamma(uint32_t i, led_color_t color, uint8_t priority);
void led_drive(uint32_t i, ids_t id);

void led_debug_set_red(void);
void led_debug_clr_red(void);
void led_debug_set_green(void);
void led_debug_clr_green(void);

void led_blink(uint32_t channel, uint32_t stage, uint8_t priority);
uint32_t change_ch(uint32_t ch, int32_t offset);

void pwm_led_init(uint8_t led_pwm_nmb, TIM_HandleTypeDef *tim_handle_ptr, uint16_t channel);
void pwm_led_set(uint8_t led_pwm_nmb, uint16_t value);

void led_set_color_and_drive(const pattern_t *pattern, uint8_t priority);

void scenes_init(void);
uint8_t get_active_scene_number(void);
void set_active_scene_number(uint8_t data);
void play_scene(void);
void stop_scene(void);
bool is_play_scene(void);
void set_base_frequency(uint32_t data);
err_code_t get_scene_pattern_info(uint8_t scene_number, uint8_t pattern_number, pattern_t *pattern, uint16_t *duration);
void execute_active_scene(float temperature, uint16_t light);

bool is_update_temperature(void);
void update_temperature_value(float temp);
float get_temperature_value(void);
void execute_update_temperature(void);
bool is_update_light(void);
void update_light_value(uint16_t lig);
void execute_update_light(void);


#endif /* LEDS_H_ */
