/*
 * i2c_transactional.h
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */

#ifndef INC_I2C_TRANSACTIONAL_H_
#define INC_I2C_TRANSACTIONAL_H_

#include <stdint.h>
#include "stm32f3xx_hal.h"
#include "errors.h"
#include "ic_devices.h"
#include "main.h"

typedef void (*callback_t)(err_code_t result, void *p_user_data);

//call from an application context
void i2c_trans_init(interface_t interface,
		            I2C_HandleTypeDef *i2c_handle,
					DMA_HandleTypeDef *i2c_dmarx_handle,
					DMA_HandleTypeDef *i2c_dmatx_handle);
err_code_t i2c_trans_put_read_A(ids_t id,
		                        ic_address_t device,
		                        uint8_t *write_data,
							    uint32_t write_size,
							    uint8_t *read_data,
							    uint32_t read_size,
							    callback_t callback,
							    void *p_user_data);
err_code_t i2c_trans_put_read_B(ids_t id,
		                        ic_address_t device,
		                        uint8_t *write_data,
							    uint32_t write_size,
							    uint8_t *read_data,
							    uint32_t read_size,
							    callback_t callback,
							    void *p_user_data);
err_code_t i2c_trans_put_read_C(ids_t id,
		                        ic_address_t device,
		                        uint8_t *write_data,
							    uint32_t write_size,
							    uint8_t *read_data,
							    uint32_t read_size,
							    callback_t callback,
							    void *p_user_data);
err_code_t i2c_trans_put_write(ids_t id,
		                       ic_address_t device,
							   uint8_t *data,
							   uint32_t size,
							   callback_t callback,
							   void *p_user_data);
err_code_t i2c_trans_execute(interface_t interface);
err_code_t i2c_trans_flush(interface_t interface);
void i2c_trans_info(interface_t interface);

#endif /* INC_I2C_TRANSACTIONAL_H_ */
