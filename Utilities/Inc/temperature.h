/*
 * temperature.h
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */

#ifndef INC_TEMPERATURE_H_
#define INC_TEMPERATURE_H_

#include <stdint.h>
#include "errors.h"

err_code_t read_temperature(float *temperature);


#endif /* INC_TEMPERATURE_H_ */
