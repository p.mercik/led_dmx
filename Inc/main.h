/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f3xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

typedef enum
{
	NONE_ID         = 0x00,

	ACC_READ_ID     = 0x01,
	ACC_WRITE_ID    = 0x11,
	TEMP_READ_ID    = 0x02,
	TEMP_WRITE_ID   = 0x12,
	LIGHT_READ_ID   = 0x03,
	LIGHT_WRITE_ID  = 0x13,
	LEDCTR_READ_ID  = 0x04,
	LEDCTR_WRITE_ID = 0x14,
	TOUCH_READ_ID   = 0x05,
	TOUCH_WRITE_ID  = 0x15,

	LED_INIT_ID     = 0x80,
	LED_ACC_ID      = 0x81,
	LED_TEMP_ID     = 0x82,
	LED_LIGHT_ID    = 0x83,
	LED_GEST_CW_ID  = 0x84,
	LED_GEST_CCW_ID = 0x85,
	LED_TOUCH_L_ID  = 0x86,
	LED_TOUCH_R_ID  = 0x87,
	LED_SCENE_ID    = 0x88,
	LED_DMX_ID		= 0x89,
}ids_t;


/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

#define TEST_BOARD_SMALL				0
#define POLO_AL_BOARD_1					0
#define POLO_AL_BOARD_DMX				1

#define STATIC_ANIMATION				0
#define DYNAMIC_ANIMATION				0
#define TEMPERATURE_LEDS				0
#define LIGHT_LEDS						0

#define TEST_STATIC_1 					0
#define TEST_STATIC_2 					0
#define TEST_STATIC_3 					1

#define DMX_BREAK_TIME 					3
#define NON_RDM_FRAME					0
#define USED_DMX_SLOTS 					6
#define DMX_SLOT_COUNT_NONCHANGE		5
#define SERIAL_LENGTH   				55
#define SC_DMX							0x00
#define DMXStartAddress					1

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void art_printf(const char *fmt, ...); // custom printf() function
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define EXTI0_GEST_INT0_Pin GPIO_PIN_0
#define EXTI0_GEST_INT0_GPIO_Port GPIOF
#define EXTI0_GEST_INT0_EXTI_IRQn EXTI0_IRQn
#define EXT1_GEST_TS_Pin GPIO_PIN_1
#define EXT1_GEST_TS_GPIO_Port GPIOF
#define IN_TEMP_INT_Pin GPIO_PIN_0
#define IN_TEMP_INT_GPIO_Port GPIOA
#define OUT_RST2_Pin GPIO_PIN_1
#define OUT_RST2_GPIO_Port GPIOA
#define EXTI2_TOUCH2_INT_Pin GPIO_PIN_2
#define EXTI2_TOUCH2_INT_GPIO_Port GPIOA
#define EXTI2_TOUCH2_INT_EXTI_IRQn EXTI2_TSC_IRQn
#define TIM2_CH4_TOUCH2_LED_Pin GPIO_PIN_3
#define TIM2_CH4_TOUCH2_LED_GPIO_Port GPIOA
#define OUT_LEDG_Pin GPIO_PIN_4
#define OUT_LEDG_GPIO_Port GPIOA
#define EXTI5_ACC_INT_Pin GPIO_PIN_5
#define EXTI5_ACC_INT_GPIO_Port GPIOA
#define EXTI5_ACC_INT_EXTI_IRQn EXTI9_5_IRQn
#define OUT_LEDR_Pin GPIO_PIN_6
#define OUT_LEDR_GPIO_Port GPIOA
#define OUT_LED_ENABLE_Pin GPIO_PIN_7
#define OUT_LED_ENABLE_GPIO_Port GPIOA
#define OUT_RST1_Pin GPIO_PIN_0
#define OUT_RST1_GPIO_Port GPIOB
#define EXTI8_TOUCH1_INT_Pin GPIO_PIN_8
#define EXTI8_TOUCH1_INT_GPIO_Port GPIOA
#define EXTI8_TOUCH1_INT_EXTI_IRQn EXTI9_5_IRQn
#define TIM1_CH4_TOUCH1_LED_Pin GPIO_PIN_11
#define TIM1_CH4_TOUCH1_LED_GPIO_Port GPIOA
#define IN_LIGHT_INT_Pin GPIO_PIN_12
#define IN_LIGHT_INT_GPIO_Port GPIOA
#define USART2_RX_Pin GPIO_PIN_15
#define USART2_RX_GPIO_Port GPIOA
#define USART2_TX_Pin GPIO_PIN_3
#define USART2_TX_GPIO_Port GPIOB
#define EXTI4_GEST_INT2_Pin GPIO_PIN_4
#define EXTI4_GEST_INT2_GPIO_Port GPIOB
#define EXTI4_GEST_INT2_EXTI_IRQn EXTI4_IRQn
/* USER CODE BEGIN Private defines */

typedef struct
{
	volatile uint8_t DMXInputData			[USED_DMX_SLOTS];				//odebrane przez DMX
	volatile uint8_t RecBuf[SERIAL_LENGTH];
	volatile uint8_t Type;				//typ transmisj
	uint16_t SlotsNumber;											//liczba odczytanych slotow
} DMXHeader_STRUCT;

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
