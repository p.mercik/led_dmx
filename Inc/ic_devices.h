/*
 * ic_devices.h
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */

#ifndef IC_DEVICES_H_
#define IC_DEVICES_H_

#include <stdint.h>
#include "main.h"

typedef enum
{
	INTERFACE_I2C_0,
	INTERFACE_I2C_1,
	INTERFACE_I2C_2
}interface_t;



typedef struct
{
	interface_t interface;
	uint8_t     address;
}ic_address_t;



#if TEST_BOARD_SMALL
	#define ACC_PRESENT              0
	#define LIGHT_PRESENT            0
	#define TEMPERATURE_PRESENT      1
	#define GESTURE_PRESENT          0
	#define TOUCH_LEFT_PRESENT       0
	#define TOUCH_RIGHT_PRESENT      0
	#define LED_GROUP_PRESENT_0      1
	#define LED_GROUP_PRESENT_1      0
	#define LED_GROUP_PRESENT_2      0
	#define LED_GROUP_PRESENT_3      0
	#define LED_GROUP_PRESENT_4      1
	#define LED_GROUP_PRESENT_5      0
	#define LED_GROUP_PRESENT_6      0
	#define LED_GROUP_PRESENT_7      0
	#define LEDS_VERSION_4
	#define LEDS_COUNT               16
#endif

#if POLO_AL_BOARD_1
	#define ACC_PRESENT              1
	#define LIGHT_PRESENT            1
	#define TEMPERATURE_PRESENT      1
	#define GESTURE_PRESENT          1
	#define TOUCH_LEFT_PRESENT       1
	#define TOUCH_RIGHT_PRESENT      1
	#define LED_GROUP_PRESENT_0      1
	#define LED_GROUP_PRESENT_1      1
	#define LED_GROUP_PRESENT_2      1
	#define LED_GROUP_PRESENT_3      1
	#define LED_GROUP_PRESENT_4      1
	#define LED_GROUP_PRESENT_5      1
	#define LED_GROUP_PRESENT_6      1
	#define LED_GROUP_PRESENT_7      1
	#define LEDS_VERSION_2
	#define LEDS_COUNT               64
#endif

#if POLO_AL_BOARD_DMX
	#define ACC_PRESENT              0
	#define LIGHT_PRESENT            0
	#define TEMPERATURE_PRESENT      0
	#define GESTURE_PRESENT          0
	#define TOUCH_LEFT_PRESENT       0
	#define TOUCH_RIGHT_PRESENT      0
	#define LED_GROUP_PRESENT_0      1
	#define LED_GROUP_PRESENT_1      1
	#define LED_GROUP_PRESENT_2      1
	#define LED_GROUP_PRESENT_3      1
	#define LED_GROUP_PRESENT_4      1
	#define LED_GROUP_PRESENT_5      1
	#define LED_GROUP_PRESENT_6      1
	#define LED_GROUP_PRESENT_7      1
	#define LEDS_VERSION_2
	#define LEDS_COUNT               64
#endif

//led
#if LED_GROUP_PRESENT_0
	#define LP5024_ADDR_0 0x50
	ic_address_t lp5024_0_addr;
#endif
#if LED_GROUP_PRESENT_1
	#define LP5024_ADDR_1 0x52
	ic_address_t lp5024_1_addr;
#endif
#if LED_GROUP_PRESENT_2
	#define LP5024_ADDR_2 0x54
	ic_address_t lp5024_2_addr;
#endif
#if LED_GROUP_PRESENT_3
	#define LP5024_ADDR_3 0x56
	ic_address_t lp5024_3_addr;
#endif
#if LED_GROUP_PRESENT_4
	#define LP5024_ADDR_4 0x50
	ic_address_t lp5024_4_addr;
#endif
#if LED_GROUP_PRESENT_5
	#define LP5024_ADDR_5 0x52
	ic_address_t lp5024_5_addr;
#endif
#if LED_GROUP_PRESENT_6
	#define LP5024_ADDR_6 0x54
	ic_address_t lp5024_6_addr;
#endif
#if LED_GROUP_PRESENT_7
	#define LP5024_ADDR_7 0x56
	ic_address_t lp5024_7_addr;
#endif


//touch
#if TOUCH_LEFT_PRESENT
	#define MTCH112_ADDR_1 0xE6
	ic_address_t mtch112_1_addr;
#endif

#if TOUCH_RIGHT_PRESENT
	#define MTCH112_ADDR_2 0xE6
	ic_address_t mtch112_2_addr;
#endif


//gest
#if GESTURE_PRESENT
    #define MGC3130_ADDR 0x84
    ic_address_t mgc3130_addr;
#endif


//accelerometer
#if ACC_PRESENT
	#define LIS2DH12_ADDR 0x30
	ic_address_t lis2dh12_addr;
#endif

//light
#if LIGHT_PRESENT
	#define TSL2569_ADDR 0x72
	ic_address_t tsl2569_addr;
#endif

//temperature
#if TEMPERATURE_PRESENT
    #define AT30TS74_ADDR 0x90
	ic_address_t at30ts74_addr;
#endif



#endif /* IC_DEVICES_H_ */
