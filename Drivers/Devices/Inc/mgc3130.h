/*
 * mgc3130.h
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */

#ifndef DEVICES_INC_MGC3130_H_
#define DEVICES_INC_MGC3130_H_

#include "../../../Utilities/Inc/errors.h"
#include "ic_devices.h"


err_code_t mgc3130_Init(ic_address_t device);
err_code_t mgc3130_WhoAmI(ic_address_t device);


#endif /* DEVICES_INC_MGC3130_H_ */
