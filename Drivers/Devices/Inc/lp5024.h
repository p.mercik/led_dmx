/*
 * lp5024.h
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */

#ifndef DEVICES_INC_LP5024_H_
#define DEVICES_INC_LP5024_H_

#include "../../../Utilities/Inc/errors.h"
#include "ic_devices.h"
#include "main.h"

typedef struct
{
	uint8_t ch0;
	uint8_t ch1;
	uint8_t ch2;
	uint8_t intensity;
}led_channel_value_t;


err_code_t lp5024_Init(ic_address_t device);
err_code_t lp5024_LedOn(ic_address_t device, uint8_t channel, led_channel_value_t value, ids_t id);
err_code_t lp5024_WhoAmI(ic_address_t device);

#endif /* DEVICES_INC_LP5024_H_ */
