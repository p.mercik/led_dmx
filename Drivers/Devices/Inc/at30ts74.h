/*
 * at30ts74.h
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */

#ifndef DEVICES_INC_AT30TS74_H_
#define DEVICES_INC_AT30TS74_H_

#include "../../../Utilities/Inc/errors.h"
#include "ic_devices.h"

err_code_t at30ts74_Init(ic_address_t device);
err_code_t at30ts74_WhoAmI(ic_address_t device);
err_code_t at30ts74_ReadTemperature(ic_address_t device, float *temperature);


#endif /* DEVICES_INC_AT30TS74_H_ */
