/*
 * lp5024.c
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */

#include <stddef.h>
#include "main.h"
#include "../Inc/lp5024.h"
#include "../../../Utilities/Inc/i2c_transactional.h"

static err_code_t lp5024_WriteRegister(ids_t id, ic_address_t device, uint8_t addr, uint8_t *data, uint8_t size)
{
	#define LP5024_BUFFER_WR_SIZE 6

	uint8_t buffer[LP5024_BUFFER_WR_SIZE];

	if (data==NULL)
	{
		art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
		return RESULT_BAD_PARAMETER;
	}

	if ((size+1)>LP5024_BUFFER_WR_SIZE)
	{
		art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
		return RESULT_BAD_PARAMETER;
	}

	buffer[0] = addr;
	for (uint32_t i=0;i<size;i++)
	{
		buffer[i+1] = data[i];
	}

	return i2c_trans_put_write(id, device, buffer, size+1, NULL, NULL);
}

static err_code_t lp5024_ReadRegister(ids_t id, ic_address_t device, uint8_t addr, uint8_t *data, uint32_t size)
{
	uint8_t addr_data;

	if (data==NULL)
	{
		art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
		return RESULT_BAD_PARAMETER;
	}

	addr_data = addr;

	return i2c_trans_put_read_A(id, device, &addr_data, 1, data, size, NULL, NULL);
}


err_code_t lp5024_Init(ic_address_t device)
{
	uint8_t data = 0x40;
	return lp5024_WriteRegister(LEDCTR_WRITE_ID, device, 0x00, &data, 1);
}

err_code_t lp5024_WhoAmI(ic_address_t device)
{
	err_code_t err_code = RESULT_ERROR;
	uint8_t data;

	err_code = lp5024_ReadRegister(LEDCTR_READ_ID, device, 0x01, &data, 1);
	if (err_code == RESULT_OK)
	{
		err_code = i2c_trans_flush(device.interface);
		if (err_code == RESULT_OK)
		{
			art_printf("[%s] (%d) device %d : data=0x%02X\r\n",__func__,__LINE__,device.address, data);
		}
		else
		{
			art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
		}
	}
	else
	{
		art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
	}

	return err_code;
}

err_code_t lp5024_LedOn(ic_address_t device, uint8_t channel, led_channel_value_t value, ids_t id)
{
	uint8_t buffer[3];

	buffer[0] = value.ch0;
	buffer[1] = value.ch1;
	buffer[2] = value.ch2;

	return lp5024_WriteRegister(id, device, 0x0F + (channel*3), buffer, 3);
}




