/*
 * tsl2569.c
 *
 *  Created on: 11.01.2019
 *      Author: PLARZAW1
 */

#include "main.h"
#include "../Inc/tsl2569.h"
#include "../../../Utilities/Inc/i2c_transactional.h"


static err_code_t tsl2569_WriteRegister(ids_t id, ic_address_t device, uint8_t addr, uint8_t data)
{
	uint8_t buffer[3];

	buffer[0] = (addr & TSL2569_REG_MASK__COMMAND__ADDRESS) | TSL2569_REG_VALUE__COMMAND__CMD__ENABLE;
	buffer[1] = data;

	return i2c_trans_put_write(id, device, buffer, 2, NULL, NULL);
}

static err_code_t tsl2569_ReadRegister(ids_t id, ic_address_t device, uint8_t addr, uint8_t *data, uint32_t size)
{
	uint8_t addr_data;

	if (data==NULL)
	{
		art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
		return RESULT_BAD_PARAMETER;
	}

	if ((size != 1) && (size != 2))
	{
		art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
		return RESULT_BAD_PARAMETER;
	}

	addr_data = (addr & TSL2569_REG_MASK__COMMAND__ADDRESS) | TSL2569_REG_VALUE__COMMAND__CMD__ENABLE;
	if (size == 2)
	{
		addr_data |= TSL2569_REG_VALUE__COMMAND__WORD__ENABLE;
	}

	return i2c_trans_put_read_A(id, device, &addr_data, 1, data, size, NULL, NULL);
}


err_code_t tsl2569_Init(ic_address_t device)
{
	tsl2569_WriteRegister(LIGHT_WRITE_ID, device, TSL2569_REG_ADDR__CONTROL, TSL2569_REG_VALUE__CONTROL__POWER__POWERUP);

	return RESULT_OK;
}

err_code_t tsl2569_WhoAmI(ic_address_t device)
{
	err_code_t err_code = RESULT_ERROR;
	uint8_t data;

	err_code = tsl2569_ReadRegister(LIGHT_READ_ID, device, TSL2569_REG_ADDR__ID, &data, 1);
	if (err_code == RESULT_OK)
	{
		err_code = i2c_trans_flush(device.interface);
		if (err_code == RESULT_OK)
		{
			art_printf("[%s] (%d) data=0x%02X\r\n",__func__,__LINE__,data);
		}
		else
		{
			art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
		}
	}
	else
	{
		art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
	}

	return err_code;
}

err_code_t tsl2569_ReadLight(ic_address_t device, uint16_t *value_all, uint16_t *value_ir)
{
	err_code_t err_code = RESULT_ERROR;
	uint8_t data[2];

	do
	{
		err_code = tsl2569_ReadRegister(LIGHT_READ_ID, device, TSL2569_REG_ADDR__DATA0_LOW, data, 2);
		if (err_code == RESULT_OK)
		{
			err_code = i2c_trans_flush(device.interface);
			if (err_code == RESULT_OK)
			{
				*value_all = ( (((uint16_t)data[0])     ) & 0x00FF ) |
							 ( (((uint16_t)data[1]) << 8) & 0xFF00 );
			}
		}
		else
		{
			break;
		}

		err_code = tsl2569_ReadRegister(LIGHT_READ_ID, device, TSL2569_REG_ADDR__DATA1_LOW, data, 2);
		if (err_code == RESULT_OK)
		{
			err_code = i2c_trans_flush(device.interface);
			if (err_code == RESULT_OK)
			{
				*value_ir = ( (((uint16_t)data[0])     ) & 0x00FF ) |
							 ( (((uint16_t)data[1]) << 8) & 0xFF00 );
			}
		}
		else
		{
			break;
		}
	}
	while(0);

	return err_code;
}

