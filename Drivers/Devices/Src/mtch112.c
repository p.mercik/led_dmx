/*
 * mtch112.c
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */

#include <stdbool.h>
#include "main.h"
#include "../Inc/mtch112.h"
#include "../../../Utilities/Inc/i2c_transactional.h"

static uint8_t mtch112_xor(uint8_t *data, uint32_t size)
{
	uint8_t result = 0;

	for (uint32_t i=0; i<size; i++)
	{
		result ^= data[i];
	}

	return result;
}

static err_code_t mtch112_WriteRegister(ids_t id, ic_address_t device, uint8_t addr, uint8_t data, uint8_t size)
{
    #define MTCH112_WRITE_BUFFER_SIZE 5
	uint8_t buffer[MTCH112_WRITE_BUFFER_SIZE];

	buffer[0] = 0x55;
	buffer[1] = 0xAA;
	buffer[2] = addr;
	buffer[3] = data;
	buffer[4] = mtch112_xor(&buffer[0],4);

	return i2c_trans_put_write(id, device, buffer, MTCH112_WRITE_BUFFER_SIZE, NULL, NULL);
}

static err_code_t mtch112_ReadRegister(ids_t id, ic_address_t device, uint8_t addr, uint8_t *data, uint32_t size)
{
	uint8_t addr_data;

	if (data==NULL)
	{
		art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
		return RESULT_BAD_PARAMETER;
	}

	addr_data = addr;

	return i2c_trans_put_read_A(id, device, &addr_data, 1, data, size, NULL, NULL);
}


err_code_t mtch112_Init(ic_address_t device)
{
	//uint8_t value;
	//uint8_t addr;

	do
	{
		if (mtch112_WriteRegister(TOUCH_WRITE_ID,
				                  device,
				                  MTCH112_REG_ADDR__OUTCON,
								  MTCH112_REG_MASK__OUTCON__S1BOE | MTCH112_REG_MASK__OUTCON__S0BOE,
								  1) != RESULT_OK)
		{
			art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
			break;
		}

		if (mtch112_WriteRegister(TOUCH_WRITE_ID, device, MTCH112_REG_ADDR__LPCON, MTCH112_REG_VALUE__LPCON__SLEEP__1MS, 1) != RESULT_OK)
		{
			art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
			break;
		}

		if (mtch112_WriteRegister(TOUCH_WRITE_ID, device, MTCH112_REG_ADDR__TIMEOUT_L, 0xFF, 1) != RESULT_OK)
		{
			art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
			break;
		}

		if (mtch112_WriteRegister(TOUCH_WRITE_ID, device, MTCH112_REG_ADDR__TIMEOUT_H, 0x00, 1) != RESULT_OK)
		{
			art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
			break;
		}
	}
	while(0);

	return RESULT_OK;
}

err_code_t mtch112_WhoAmI(ic_address_t device)
{
	err_code_t err_code = RESULT_ERROR;
	uint8_t data;

	err_code = mtch112_ReadRegister(TOUCH_READ_ID, device, 0x0B, &data, 1);
	if (err_code == RESULT_OK)
	{
		err_code = i2c_trans_flush(device.interface);
		if (err_code == RESULT_OK)
		{
			art_printf("[%s] (%d) data=0x%02X\r\n",__func__,__LINE__,data);
		}
		else
		{
			art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
		}
	}
	else
	{
		art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
	}

	return err_code;
}


err_code_t mtch112_ReadState(ic_address_t device,
		                     mtch112_error_state_t *error_state,
							 bool *sen0_btn,
							 bool *sen1_btn,
							 bool *sen0_pxm,
							 bool *sen1_pxm)
{
	err_code_t err_code = RESULT_ERROR;
	uint8_t data;

	err_code = mtch112_ReadRegister(TOUCH_READ_ID, device, MTCH112_REG_ADDR__STATE, &data, 1);
	if (err_code == RESULT_OK)
	{
		err_code = i2c_trans_flush(device.interface);
		if (err_code == RESULT_OK)
		{
			//art_printf("[%s] (%d) data=0x%02X\r\n",__func__,__LINE__,data);

			switch (data & MTCH112_REG_MASK__STATE__ERRSTATE)
			{
				case MTCH112_REG_VALUE__STATE__BOTH_OK:					*error_state = MTCH112_STATE_BOTH_OK;				break;
				case MTCH112_REG_VALUE__STATE__SEN0_SHORTED_VDD:		*error_state = MTCH112_STATE_SEN0_SHORTED_VDD;		break;
				case MTCH112_REG_VALUE__STATE__SEN1_SHORTED_VDD:		*error_state = MTCH112_STATE_SEN1_SHORTED_VDD;		break;
				case MTCH112_REG_VALUE__STATE__SEN0_SHORTED_VSS:		*error_state = MTCH112_STATE_SEN0_SHORTED_VSS;		break;
				case MTCH112_REG_VALUE__STATE__SEN1_SHORTED_VSS:		*error_state = MTCH112_STATE_SEN1_SHORTED_VSS;		break;
				case MTCH112_REG_VALUE__STATE__BOTH_SHORTED_TOGETHER:	*error_state = MTCH112_STATE_BOTH_SHORTED_TOGETHER;	break;
				default:												*error_state = MTCH112_STATE_BOTH_OK;  				break;
			}

			*sen0_btn = false;
			*sen1_btn = false;
			*sen0_pxm = false;
			*sen1_pxm = false;
			if ((data & MTCH112_REG_MASK__STATE__S0BS) == MTCH112_REG_VALUE__STATE__S0BS__DETECTED) *sen0_btn = true;
			if ((data & MTCH112_REG_MASK__STATE__S1BS) == MTCH112_REG_VALUE__STATE__S1BS__DETECTED) *sen1_btn = true;
			if ((data & MTCH112_REG_MASK__STATE__S0PS) == MTCH112_REG_VALUE__STATE__S0PS__DETECTED) *sen0_pxm = true;
			if ((data & MTCH112_REG_MASK__STATE__S1PS) == MTCH112_REG_VALUE__STATE__S1PS__DETECTED) *sen1_pxm = true;

		}
		else
		{
			art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
		}
	}
	else
	{
		art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
	}

	return err_code;



}

