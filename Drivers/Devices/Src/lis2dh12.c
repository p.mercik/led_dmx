/*
 * lis2dh12.c
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */

#include "main.h"
#include "../Inc/lis2dh12.h"
#include "../../../Utilities/Inc/i2c_transactional.h"


#define CLICK_THS_VALUE             0x60
#define CLICK_TIMELIMIT_VALUE       0x18
#define CLICK_TIMELATENCY_VALUE     0x20    //TODO
#define ORIENTATION_THS_VALUE       0x66
#define ORIENTATION_DURATION_VALUE  96



static err_code_t lis2dh12_WriteRegister(ic_address_t device, uint8_t addr, uint8_t data)
{
    uint8_t buffer[2];

	buffer[0] = addr;
	buffer[1] = data;

	return i2c_trans_put_write(ACC_WRITE_ID, device, buffer, 2, NULL, NULL);
}

static err_code_t lis2dh12_ReadRegister(ic_address_t device, uint8_t addr, uint8_t *data, uint32_t size)
{
	uint8_t addr_data;

	if (data==NULL)
	{
		art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
		return RESULT_BAD_PARAMETER;
	}

	addr_data = addr;

	return i2c_trans_put_read_A(ACC_READ_ID, device, &addr_data, 1, data, size, NULL, NULL);
}

err_code_t lis2dh12_ReadInterrupt(ic_address_t device)
{
	err_code_t err_code = RESULT_ERROR;
	uint8_t data[2];

	err_code = lis2dh12_ReadRegister(device, LIS2DH12_REG_ADDR__INT1_SRC, data, 1);		// 0x33
	if (err_code == RESULT_OK)
	{
		err_code = i2c_trans_flush(device.interface);
		if (err_code == RESULT_OK)
		{
			art_printf("[%s] (%d) data=0x%04X\r\n",__func__,__LINE__,data[0],data[1]);
		}
		else
		{
			art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
		}
	}
	else
	{
		art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
	}

	err_code = lis2dh12_ReadRegister(device, LIS2DH12_REG_ADDR__CLICK_SRC, data, 1);		// 0x33
	if (err_code == RESULT_OK)
	{
		err_code = i2c_trans_flush(device.interface);
		if (err_code == RESULT_OK)
		{
			art_printf("[%s] (%d) data=0x%04X\r\n",__func__,__LINE__,data[0],data[1]);
		}
		else
		{
			art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
		}
	}
	else
	{
		art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
	}

	return err_code;

}


err_code_t lis2dh12_WhoAmI(ic_address_t device)
{
	err_code_t err_code = RESULT_ERROR;
	uint8_t data[2];

	err_code = lis2dh12_ReadRegister(device, LIS2DH12_REG_ADDR__WHOAMI, data, 1);		// 0x33
	if (err_code == RESULT_OK)
	{
		err_code = i2c_trans_flush(device.interface);
		if (err_code == RESULT_OK)
		{
			art_printf("[%s] (%d) data=0x%04X\r\n",__func__,__LINE__,data[0],data[1]);
		}
		else
		{
			art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
		}
	}
	else
	{
		art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
	}

	return err_code;
}


err_code_t lis2dh12_Init(ic_address_t device)
{
	err_code_t err_code;

	do
	{
		// single tap
		// 1. HP for click ON in CTRL_REG2                                 (V)
		// 2. activate I1_CLICK in CTRL_REG3                               (V)
		// 3. set ZS, YS, XS in CLICK_CFG                                  (V)
		// 4. set THS in CLICK_THS <- if FS=2G, THS=0.75G => THS=0x60      (V)
		// 5. set TLI in TIME_LIMIT <- TLI=0x18 for ODR=400Hz              (V)
		// 6. set TLA in TIME_LATENCY                                      (V)
		// 7. start sensor with ODR=400Hz (CTR_REG1) and HR=1 in CTRL_REG4 (V)

		// position
		// 1. set I1_IA1 in CTRL_REG3                                      (V)
		// 2. set bits 6D, ZHIE, ZLIE, YHIE, YLIE, XHIE, XLIE in INT1_CFG  (V)
		// 3. set THS in INT1_THS <- THS=0x66 (54*16mg=864mg)              (V)
		// 4. set D in INT1_DURATION <- D=96 (96*(1/400Hz) = 240 ms)       (V)
		// 5. start with ODR=400 and LPEN in CTRL_REG1                     (V)
		//
		// together:
		// CTRL_REG2 <= LIS2DH12_REG_VALUE__CTRL_REG2__HPCLICK__ENABLE
		// CTRL_REG3 <= LIS2DH12_REG_VALUE__CTRL_REG3__I1_CLICK__ENABLE
		//              LIS2DH12_REG_VALUE__CTRL_REG3__I1_IA1__ENABLE
		// CLICK_CFG <= LIS2DH12_REG_VALUE__CLICK_CFG__XS__ENABLE
		//              LIS2DH12_REG_VALUE__CLICK_CFG__YS__ENABLE
		//              LIS2DH12_REG_VALUE__CLICK_CFG__ZS__ENABLE
		// CLICK_THS <= 0x60
		// TIME_LIMIT <= 0x18
		// TIME_LATENCY <= ?
		// INT1_CFG <= LIS2DH12_REG_VALUE__INT1_CFG__6D__ENABLE
		//             LIS2DH12_REG_VALUE__INT1_CFG__ZHIE__ENABLE
		//             LIS2DH12_REG_VALUE__INT1_CFG__ZLIE__ENABLE
		//             LIS2DH12_REG_VALUE__INT1_CFG__YHIE__ENABLE (?)
		//             LIS2DH12_REG_VALUE__INT1_CFG__YLIE__ENABLE (?)
		//             LIS2DH12_REG_VALUE__INT1_CFG__XHIE__ENABLE (?)
		//             LIS2DH12_REG_VALUE__INT1_CFG__XLIE__ENABLE (?)
		// INT1_THS <= 0x66
		// INT1_DURATION <= 96
		// CTRL_REG4 <= LIS2DH12_REG_VALUE__CTRL_REG4__HR__1
		// CTRL_REG1 <= LIS2DH12_REG_VALUE__CTRL_REG1__ODR__400HZ
		//              LIS2DH12_REG_VALUE__CTRL_REG1__ZEN__ENABLED
		//              LIS2DH12_REG_VALUE__CTRL_REG1__YEN__ENABLED
		//              LIS2DH12_REG_VALUE__CTRL_REG1__XEN__ENABLED
		//

		if ((err_code = lis2dh12_WriteRegister(device, LIS2DH12_REG_ADDR__CTRL_REG2,
											           LIS2DH12_REG_VALUE__CTRL_REG2__HPCLICK__ENABLE)) != RESULT_OK)
		{
			break;
		}

		if ((err_code = lis2dh12_WriteRegister(device, LIS2DH12_REG_ADDR__CTRL_REG3,
											           LIS2DH12_REG_VALUE__CTRL_REG3__I1_CLICK__ENABLE/* |
											           LIS2DH12_REG_VALUE__CTRL_REG3__I1_IA1__ENABLE*/)) != RESULT_OK)
		{
			break;
		}

		if ((err_code = lis2dh12_WriteRegister(device, LIS2DH12_REG_ADDR__CLICK_CFG,
				                                       LIS2DH12_REG_VALUE__CLICK_CFG__XS__ENABLE |
													   LIS2DH12_REG_VALUE__CLICK_CFG__YS__ENABLE |
													   LIS2DH12_REG_VALUE__CLICK_CFG__ZS__ENABLE)) != RESULT_OK)
		{
			break;
		}

		if ((err_code = lis2dh12_WriteRegister(device, LIS2DH12_REG_ADDR__CLICK_THS, CLICK_THS_VALUE)) != RESULT_OK)
		{
			break;
		}

		if ((err_code = lis2dh12_WriteRegister(device, LIS2DH12_REG_ADDR__TIME_LIMIT, CLICK_TIMELIMIT_VALUE)) != RESULT_OK)
		{
			break;
		}

		if ((err_code = lis2dh12_WriteRegister(device, LIS2DH12_REG_ADDR__TIME_LATENCY, CLICK_TIMELATENCY_VALUE)) != RESULT_OK)
		{
			break;
		}

		/*if ((err_code = lis2dh12_WriteRegister(device, LIS2DH12_REG_ADDR__INT1_CFG,
													   LIS2DH12_REG_VALUE__INT1_CFG__6D__ENABLE	  |
													   LIS2DH12_REG_VALUE__INT1_CFG__ZHIE__ENABLE |
											           LIS2DH12_REG_VALUE__INT1_CFG__ZLIE__ENABLE |
													   LIS2DH12_REG_VALUE__INT1_CFG__YHIE__ENABLE |
													   LIS2DH12_REG_VALUE__INT1_CFG__YLIE__ENABLE |
													   LIS2DH12_REG_VALUE__INT1_CFG__XHIE__ENABLE |
                                                       LIS2DH12_REG_VALUE__INT1_CFG__XLIE__ENABLE)) != RESULT_OK)
		{
			break;
		}*/

		if ((err_code = lis2dh12_WriteRegister(device, LIS2DH12_REG_ADDR__INT1_THS, ORIENTATION_THS_VALUE)) != RESULT_OK)
		{
			break;
		}

		if ((err_code = lis2dh12_WriteRegister(device, LIS2DH12_REG_ADDR__INT1_DURATION, ORIENTATION_DURATION_VALUE)) != RESULT_OK)
		{
			break;
		}

		if ((err_code = lis2dh12_WriteRegister(device, LIS2DH12_REG_ADDR__CTRL_REG4,
				                                       LIS2DH12_REG_VALUE__CTRL_REG4__HR__1)) != RESULT_OK)
		{
			break;
		}

		if ((err_code = lis2dh12_WriteRegister(device, LIS2DH12_REG_ADDR__CTRL_REG1,
				                                       LIS2DH12_REG_VALUE__CTRL_REG1__ODR__400HZ |
								                       LIS2DH12_REG_VALUE__CTRL_REG1__ZEN__ENABLED |
								                       LIS2DH12_REG_VALUE__CTRL_REG1__YEN__ENABLED |
								                       LIS2DH12_REG_VALUE__CTRL_REG1__XEN__ENABLED)) != RESULT_OK)
		{
			break;
		}
	}
	while(0);

	return err_code;
}
