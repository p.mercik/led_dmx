/*
 * mgc3130.c
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */

#include "../../../Utilities/Inc/i2c_transactional.h"


/*
static err_code_t mgc3130_WriteRegister(ic_address_t device, uint8_t addr, uint8_t data)
{
    uint8_t buffer[2];

	buffer[0] = addr;
	buffer[1] = data;

	return i2c_trans_put_write(device, buffer, 2, NULL, NULL);
}
*/

/*
static err_code_t mgc3130_ReadRegister(ic_address_t device, uint8_t addr, uint8_t *data, uint32_t size)
{
	uint8_t addr_data;

	if (data==NULL)
	{
		art_printf("[%s] (%d) ERROR\r\n",__func__,__LINE__);
		return RESULT_BAD_PARAMETER;
	}

	addr_data = addr;

	return i2c_trans_put_read_A(device, &addr_data, 1, data, size, NULL, NULL);
}
*/

err_code_t mgc3130_Init(ic_address_t device)
{
	return RESULT_OK;
}

/*
err_code_t mgc3130_WhoAmI(ic_address_t device)
{
	err_code_t err_code = RESULT_ERROR;
	uint8_t data[2];

	err_code = at30ts74_ReadRegister(device, 0x02, data, 2);		// 0x4B00
	if (err_code == RESULT_OK)
	{
		err_code = i2c_trans_flush(device.interface);
		if (err_code == RESULT_OK)
		{
			art_printf("[%s] (%d) data=0x%04X\r\n",__func__,__LINE__,data[0],data[1]);
		}
		else
		{
			art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
		}
	}
	else
	{
		art_printf("[%s] (%d) ERROR %d\r\n",__func__,__LINE__,err_code);
	}

	return err_code;
}
*/

