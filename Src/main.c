/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "ic_devices.h"
#include "../Utilities/Inc/i2c_transactional.h"
#include "../Utilities/Inc/leds.h"
#include "../Utilities/Inc/temperature.h"
#include "../Drivers/Devices/Inc/lp5024.h"
#include "../Drivers/Devices/Inc/mtch112.h"
#include "../Drivers/Devices/Inc/tsl2569.h"
#include "../Drivers/Devices/Inc/at30ts74.h"
#include "../Drivers/Devices/Inc/lis2dh12.h"
#include "../Drivers/Devices/Inc/mgc3130.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */



/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c2;
DMA_HandleTypeDef hdma_i2c1_rx;
DMA_HandleTypeDef hdma_i2c1_tx;
DMA_HandleTypeDef hdma_i2c2_rx;
DMA_HandleTypeDef hdma_i2c2_tx;

IWDG_HandleTypeDef hiwdg;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim15;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
#if POLO_AL_BOARD_DMX
	static volatile DMXHeader_STRUCT DMXframe;
	static volatile bool uart_active_start = true;
	uint8_t RecBuffMab;
#endif
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_I2C2_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_IWDG_Init(void);
static void MX_TIM6_Init(void);
static void MX_TIM15_Init(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2C2_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_IWDG_Init();
  MX_TIM6_Init();
  MX_TIM15_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */


  led_debug_set_red();


  //pwm - 0..18000
  #define TOUCH_LED_OFF    17999
  #define TOUCH_LED_FULL   1
  #define TOUCH_LED_DARK   15000
  #define TOUCH LED_BRIGHT 2000

  HAL_TIMEx_PWMN_Start(&htim2, TIM_CHANNEL_4);
  pwm_led_init(LED_PWM_1, &htim2, TIM_CHANNEL_4);
  pwm_led_set(LED_PWM_1, TOUCH_LED_DARK);

  HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_4);
  pwm_led_init(LED_PWM_2, &htim1, TIM_CHANNEL_4);
  pwm_led_set(LED_PWM_2, TOUCH_LED_DARK);


  //i2c
  i2c_trans_init(INTERFACE_I2C_1, &hi2c1, &hdma_i2c1_rx, &hdma_i2c1_tx);
  i2c_trans_init(INTERFACE_I2C_2, &hi2c2, &hdma_i2c2_rx, &hdma_i2c2_tx);

  //led
  #if LED_GROUP_PRESENT_0 ||  \
      LED_GROUP_PRESENT_1 ||  \
      LED_GROUP_PRESENT_2 ||  \
      LED_GROUP_PRESENT_3 ||  \
      LED_GROUP_PRESENT_4 ||  \
      LED_GROUP_PRESENT_5 ||  \
      LED_GROUP_PRESENT_6 ||  \
      LED_GROUP_PRESENT_7
    led_init(0.8, 0.8, 0.8);
  #endif

  //touch
  #if TOUCH_LEFT_PRESENT
  	HAL_GPIO_WritePin(OUT_RST1_GPIO_Port, OUT_RST1_Pin, GPIO_PIN_SET);
  	HAL_Delay(200);
  	//mtch112_Init(mtch112_1_addr);
  #endif

  #if TOUCH_RIGHT_PRESENT
  	HAL_GPIO_WritePin(OUT_RST2_GPIO_Port, OUT_RST2_Pin, GPIO_PIN_SET);
  	HAL_Delay(200);
  	//mtch112_Init(mtch112_2_addr);
  #endif

  //gest
  #if GESTURE_PRESENT
    //HAL_GPIO_WritePin(OUT_RST3_GPIO_Port, OUT_RST3_Pin, GPIO_PIN_SET);
    //mgc3130_Init(mgc3130_addr);
  #endif

  //temperature
  #if TEMPERATURE_PRESENT
    at30ts74_Init(at30ts74_addr);
  #endif

  //light
  #if LIGHT_PRESENT
  	tsl2569_Init(tsl2569_addr);
  #endif

  #if ACC_PRESENT
    //acc
    lis2dh12_Init(lis2dh12_addr);
  #endif



  led_color_t color;
#if TEST_STATIC_3
  color.r = 255;
  color.g = 0;
  color.b = 0;
  for (uint32_t i=0; i<LEDS_COUNT; i++)
  {
	  led_set_color_pure(i, color, 0);
	  led_drive(i,LED_SCENE_ID);
	  i2c_trans_execute(INTERFACE_I2C_1);
	  i2c_trans_execute(INTERFACE_I2C_2);
  }
  for (int i=0; i<10; i++)
  {
	  //HAL_IWDG_Refresh(&hiwdg);
	  HAL_Delay(200);
  }

  color.r = 0;
  color.g = 255;
  color.b = 0;
  for (uint32_t i=0; i<LEDS_COUNT; i++)
  {
	  led_set_color_pure(i, color, 0);
	  led_drive(i,LED_SCENE_ID);
	  i2c_trans_execute(INTERFACE_I2C_1);
	  i2c_trans_execute(INTERFACE_I2C_2);
  }
  for (int i=0; i<10; i++)
  {
	  //HAL_IWDG_Refresh(&hiwdg);
	  HAL_Delay(200);
  }

  color.r = 0;
  color.g = 0;
  color.b = 255;
  for (uint32_t i=0; i<LEDS_COUNT; i++)
  {
	  led_set_color_pure(i, color, 0);
	  led_drive(i,LED_SCENE_ID);
	  i2c_trans_execute(INTERFACE_I2C_1);
	  i2c_trans_execute(INTERFACE_I2C_2);
  }
  for (int i=0; i<10; i++)
  {
	  //HAL_IWDG_Refresh(&hiwdg);
	  HAL_Delay(200);
  }

  color.r = 255;
  color.g = 255;
  color.b = 255;
  for (uint32_t i=0; i<LEDS_COUNT; i++)
  {
	  led_set_color_pure(i, color, 0);
	  led_drive(i,LED_SCENE_ID);
	  i2c_trans_execute(INTERFACE_I2C_1);
	  i2c_trans_execute(INTERFACE_I2C_2);
  }
  for (int i=0; i<10; i++)
  {
	  //HAL_IWDG_Refresh(&hiwdg);
	  HAL_Delay(200);
  }

#endif


  color.r = 0;
  color.g = 0;
  color.b = 0;
  for (uint32_t i=0; i<LEDS_COUNT; i++)
  {
	  led_set_color_pure(i, color, 0);
	  led_drive(i, LED_INIT_ID);
  }



#if TEST_STATIC_1
  for (uint32_t i=0; i<5; i++)
  {
	  color.r = 255;
	  color.g = 255;
	  color.b = 255;
	  led_set_color(i, color, 0);
	  led_drive(i);
  }

  for (uint32_t i=5; i<10; i++)
  {
	  color.r = 255;
	  color.g = 0;
	  color.b = 0;
	  led_set_color(i, color, 0);
	  led_drive(i);
  }

  for (uint32_t i=10; i<16; i++)
  {
	  color.r = 0;
	  color.g = 255;
	  color.b = 0;
	  led_set_color(i, color, 0);
	  led_drive(i);
  }

  for (uint32_t i=48; i<53; i++)
  {
	  color.r = 0;
	  color.g = 0;
	  color.b = 255;
	  led_set_color(i, color, 0);
	  led_drive(i);
  }

  for (uint32_t i=53; i<58; i++)
  {
	  color.r = 100;
	  color.g = 100;
	  color.b = 0;
	  led_set_color(i, color, 0);
	  led_drive(i);
  }

  for (uint32_t i=58; i<63; i++)
  {
	  color.r = 5;
	  color.g = 5;
	  color.b = 5;
	  led_set_color(i, color, 0);
	  led_drive(i);
  }






  for (uint32_t i=26; i<31; i++)   //LEDS_COUNT   //48-56 - najcieplejsze
  {
	  color.r = 255;
	  color.g = 255;
	  color.b = 255;
	  led_set_color(i, color, 0);
	  led_drive(i);
  }

  for (uint32_t i=21; i<26; i++)
  {
	  color.r = 255;
	  color.g = 0;
	  color.b = 0;
	  led_set_color(i, color, 0);
	  led_drive(i);
  }

  for (uint32_t i=16; i<21; i++)
  {
	  color.r = 0;
	  color.g = 255;
	  color.b = 0;
	  led_set_color(i, color, 0);
	  led_drive(i);
  }

  for (uint32_t i=42; i<48; i++)
  {
	  color.r = 0;
	  color.g = 0;
	  color.b = 255;
	  led_set_color(i, color, 0);
	  led_drive(i);
  }

  for (uint32_t i=37; i<42; i++)
  {
	  color.r = 100;
	  color.g = 100;
	  color.b = 0;
	  led_set_color(i, color, 0);
	  led_drive(i);
  }

  for (uint32_t i=32; i<37; i++)
  {
	  color.r = 5;
	  color.g = 5;
	  color.b = 5;
	  led_set_color(i, color, 0);
	  led_drive(i);
  }
#endif


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  art_printf("V004\r\n");

  led_debug_set_red();
  for (int i=0; i<10; i++)
  {
	  //HAL_IWDG_Refresh(&hiwdg);
	  HAL_Delay(250);
  }
  led_debug_clr_red();

//led_debug_set_green();
//while(1)
//{
//	led_debug_set_green();
//	HAL_Delay(500);
//	led_debug_clr_green();
//	HAL_Delay(500);
//
//}

  #if LED_GROUP_PRESENT_0
    lp5024_WhoAmI(lp5024_0_addr);
    art_printf("lp5024_WhoAmI 1\r\n");
  #endif
  #if LED_GROUP_PRESENT_1
    lp5024_WhoAmI(lp5024_1_addr);
    art_printf("lp5024_WhoAmI 2\r\n");
  #endif
  #if LED_GROUP_PRESENT_2
    lp5024_WhoAmI(lp5024_2_addr);
    art_printf("lp5024_WhoAmI 3\r\n");
  #endif
  #if LED_GROUP_PRESENT_3
    lp5024_WhoAmI(lp5024_3_addr);
    art_printf("lp5024_WhoAmI 4\r\n");
  #endif
  #if LED_GROUP_PRESENT_4
    lp5024_WhoAmI(lp5024_4_addr);
    art_printf("lp5024_WhoAmI 5\r\n");
  #endif
  #if LED_GROUP_PRESENT_5
    lp5024_WhoAmI(lp5024_5_addr);
    art_printf("lp5024_WhoAmI 6\r\n");
  #endif
  #if LED_GROUP_PRESENT_6
    lp5024_WhoAmI(lp5024_6_addr);
    art_printf("lp5024_WhoAmI 7\r\n");
  #endif
  #if LED_GROUP_PRESENT_7
    lp5024_WhoAmI(lp5024_7_addr);
    art_printf("lp5024_WhoAmI 8\r\n");
  #endif


  #if TOUCH_LEFT_PRESENT
    //mtch112_WhoAmI(mtch112_1_addr);
    art_printf("mtch112_WhoAmI\r\n");
  #endif

  #if TOUCH_RIGHT_PRESENT
    //mtch112_WhoAmI(mtch112_2_addr);
    art_printf("mtch112_WhoAmI\r\n");
  #endif

  #if TEMPERATURE_PRESENT
    at30ts74_WhoAmI(at30ts74_addr);
    art_printf("at30ts74_WhoAmI\r\n");
  #endif

  #if LIGHT_PRESENT
    tsl2569_WhoAmI(tsl2569_addr);
    art_printf("tsl2569_WhoAmI\r\n");
  #endif

  #if ACC_PRESENT
    lis2dh12_WhoAmI(lis2dh12_addr);
    art_printf("lis2dh12_WhoAmI\r\n");
  #endif

  #if GESTURE_PRESENT
    //mgc3130_WhoAmI(mgc3130_addr);
    //art_printf("mgc3130_WhoAmI\r\n");
  #endif



  //scenes
#if TEST_STATIC_1
#else
#if TEST_STATIC_2
#else

  #if DYNAMIC_ANIMATION
    HAL_TIM_Base_Start_IT(&htim6);
    set_base_frequency(150);		//it must be equal to TIM6 base
    scenes_init();
    set_active_scene_number(0);
    //  play_scene();
  #endif
#endif
#endif


  led_debug_set_green();

  #if STATIC_ANIMATION
	  uint32_t ch1=0;
	  uint32_t ch2=0;
	  uint32_t ch3=0;
	  uint32_t ch4=0;
	  uint32_t st=0;
  #endif


  uint32_t counter = 0;
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  //HAL_IWDG_Refresh(&hiwdg);

	  //__disable_irq();

	  #if POLO_AL_BOARD_DMX
	    while(uart_active_start)
	    {
		    HAL_UART_Receive_IT(&huart2, &RecBuffMab, 1);
	    }
      #endif

	  err_code_t e1, e2;
	  e1 = i2c_trans_execute(INTERFACE_I2C_1);
	  e2 = i2c_trans_execute(INTERFACE_I2C_2);

	  //__enable_irq();

	  if ((e1 == RESULT_IDLE) && (e2 == RESULT_IDLE))
	  {
		#if STATIC_ANIMATION
		  HAL_Delay(3);
        #endif

		#if DYNAMIC_ANIMATION
		  HAL_Delay(50);
		  if (counter<1200)
		  {
			  counter++;
			  if (counter==1200)    //1200)
			  {
				  //play_scene();
			  }
//			  continue;
		  }
		#endif

		#if TEST_STATIC_1
		  continue;
		#endif

		#if TEST_STATIC_2
		  continue;
		#endif



		#if 0
		  HAL_Delay(300);

		  mtch112_error_state_t err_state;
		  bool s10_btn, s11_btn, s10_pxm, s11_pxm;
		  bool s20_btn, s21_btn, s20_pxm, s21_pxm;
		  if ((mtch112_ReadState(mtch112_0_addr, &err_state, &s10_btn, &s11_btn, &s10_pxm, &s11_pxm) == RESULT_OK) &&
		      (mtch112_ReadState(mtch112_1_addr, &err_state, &s20_btn, &s21_btn, &s20_pxm, &s21_pxm) == RESULT_OK) )
		  {
			  art_printf("touch: %d %d %d %d %d - %d %d %d %d \r\n",err_state,
					  s10_btn, s11_btn, s10_pxm, s11_pxm,
					  s20_btn, s21_btn, s20_pxm, s21_pxm);
		  }

		  float temperature;
		  if (at30ts74_ReadTemperature(at30ts74_addr, &temperature) == RESULT_OK)
		  {
		  	  art_printf("temperature = %d \r\n",(int)temperature);
		  }

		  uint16_t light1, light2;
		  if (tsl2569_ReadLight(tsl2569_addr, &light1, &light2) == RESULT_OK)
		  {
			  art_printf("light: %d %d \r\n",light1, light2);
		  }
		#endif


		#if 0
		  //touch print
		  mtch112_error_state_t err_state;
		  bool s10_btn, s11_btn, s10_pxm, s11_pxm;
		  bool s20_btn, s21_btn, s20_pxm, s21_pxm;
		  if ((mtch112_ReadState(mtch112_0_addr, &err_state, &s10_btn, &s11_btn, &s10_pxm, &s11_pxm) == RESULT_OK) &&
		      (mtch112_ReadState(mtch112_1_addr, &err_state, &s20_btn, &s21_btn, &s20_pxm, &s21_pxm) == RESULT_OK) )
		  {
			  art_printf("touch: %d %d %d %d %d - %d %d %d %d \r\n",err_state,
					  s10_btn, s11_btn, s10_pxm, s11_pxm,
					  s20_btn, s21_btn, s20_pxm, s21_pxm);
		  }
		#endif


		#if ACC_PRESENT
		    lis2dh12_ReadInterrupt(lis2dh12_addr);
		#endif


		#if TEMPERATURE_PRESENT
		  // temperature
		  float temperature;

		  if (at30ts74_ReadTemperature(at30ts74_addr, &temperature) == RESULT_OK)
		  {
		      #if TEMPERATURE_LEDS
			  	  update_temperature_value(temperature);
			  #endif
		  }
		#endif

		#if LIGHT_PRESENT
		  //light
		  uint16_t light1, light2;
		  if (tsl2569_ReadLight(tsl2569_addr, &light1, &light2) == RESULT_OK)
		  {
			  #if LIGHT_LEDS
			  	  update_light_value(light1);
			  #endif
		  }
		#endif


		#if STATIC_ANIMATION
		  //blink
		  led_blink(ch1, st, LED_PRIORITY_BLINK);
		  //led_blink(ch2, st, LED_PRIORITY_BLINK);
		  //led_blink(ch3, st, LED_PRIORITY_BLINK);
		  //led_blink(ch4, st, LED_PRIORITY_BLINK);

		  led_drive(ch1,LED_SCENE_ID);
		  //led_drive(ch2);
		  //led_drive(ch3);
		  //led_drive(ch4);

		  st+=5;
		  //st+=10;
		  if (st>1540)
		  {
			  st=0;

			  ch1 = change_ch(ch1, 1);
			  //ch2 = change_ch(ch2, -1);
			  //ch3 = change_ch(ch3, 1);
			  //ch4 = change_ch(ch4, -1);
		  }
		#endif

	  }


/*
	  if ((i2c_trans_execute(INTERFACE_I2C_1) == RESULT_IDLE) &&
		  (i2c_trans_execute(INTERFACE_I2C_2) == RESULT_IDLE))
	  {
		  char sign;
		  if (HAL_UART_Receive(&huart2, (uint8_t*)(&sign), 1, 500) == HAL_OK)
		  {
			  if (buffer_cnt < MAX_BUFFER_CNT)
			  {
				  buffer[buffer_cnt] = sign;
				  buffer_cnt++;
			  }

			  art_printf("%c",sign);

			  if (sign == 0x0D)
			  {
				  //execute
				  buffer_cnt = 0;


				  uint32_t channel;

				  led_debug_set_red();

				  //cc-rrr-ggg-bbb
				  channel = atoi(&buffer[0]);
				  color.r = atoi(&buffer[3]);
				  color.g = atoi(&buffer[7]);
				  color.b = atoi(&buffer[11]);

				  led_set_color(channel, color);
				  led_drive(channel);

				  HAL_Delay(500);

				  led_debug_clr_red();
			  }


		  }
	  }
*/
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_I2C2
                              |RCC_PERIPHCLK_TIM1|RCC_PERIPHCLK_TIM15;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  PeriphClkInit.I2c2ClockSelection = RCC_I2C2CLKSOURCE_HSI;
  PeriphClkInit.Tim1ClockSelection = RCC_TIM1CLK_HCLK;
  PeriphClkInit.Tim15ClockSelection = RCC_TIM15CLK_HCLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* EXTI0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);
  /* EXTI2_TSC_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI2_TSC_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI2_TSC_IRQn);
  /* EXTI9_5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
  /* TIM6_DAC_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM6_DAC_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);
  /* EXTI4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_IRQn);
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x0000020B;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.Timing = 0x0000020B;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief IWDG Initialization Function
  * @param None
  * @retval None
  */
static void MX_IWDG_Init(void)
{

  /* USER CODE BEGIN IWDG_Init 0 */

  /* USER CODE END IWDG_Init 0 */

  /* USER CODE BEGIN IWDG_Init 1 */

  /* USER CODE END IWDG_Init 1 */
  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_16;
  hiwdg.Init.Window = 4095;
  hiwdg.Init.Reload = 4095;
  if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN IWDG_Init 2 */

  /* USER CODE END IWDG_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 18000;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
  sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
  sBreakDeadTimeConfig.Break2Filter = 0;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 1;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 18000;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 7999;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 50;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief TIM15 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM15_Init(void)
{

  /* USER CODE BEGIN TIM15_Init 0 */

  /* USER CODE END TIM15_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM15_Init 1 */

  /* USER CODE END TIM15_Init 1 */
  htim15.Instance = TIM15;
  htim15.Init.Prescaler = 0;
  htim15.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim15.Init.Period = 255;
  htim15.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim15.Init.RepetitionCounter = 0;
  htim15.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim15) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim15, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim15, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM15_Init 2 */

  /* USER CODE END TIM15_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 250000;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);
  /* DMA1_Channel5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);
  /* DMA1_Channel6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);
  /* DMA1_Channel7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, OUT_RST2_Pin|OUT_LED_ENABLE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, OUT_LEDG_Pin|OUT_LEDR_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(OUT_RST1_GPIO_Port, OUT_RST1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : EXTI0_GEST_INT0_Pin */
  GPIO_InitStruct.Pin = EXTI0_GEST_INT0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(EXTI0_GEST_INT0_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : EXT1_GEST_TS_Pin */
  GPIO_InitStruct.Pin = EXT1_GEST_TS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(EXT1_GEST_TS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : IN_TEMP_INT_Pin IN_LIGHT_INT_Pin */
  GPIO_InitStruct.Pin = IN_TEMP_INT_Pin|IN_LIGHT_INT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : OUT_RST2_Pin OUT_LEDG_Pin OUT_LEDR_Pin OUT_LED_ENABLE_Pin */
  GPIO_InitStruct.Pin = OUT_RST2_Pin|OUT_LEDG_Pin|OUT_LEDR_Pin|OUT_LED_ENABLE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : EXTI2_TOUCH2_INT_Pin EXTI5_ACC_INT_Pin EXTI8_TOUCH1_INT_Pin */
  GPIO_InitStruct.Pin = EXTI2_TOUCH2_INT_Pin|EXTI5_ACC_INT_Pin|EXTI8_TOUCH1_INT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : OUT_RST1_Pin */
  GPIO_InitStruct.Pin = OUT_RST1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(OUT_RST1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : EXTI4_GEST_INT2_Pin */
  GPIO_InitStruct.Pin = EXTI4_GEST_INT2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(EXTI4_GEST_INT2_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void vprint(const char *fmt, va_list argp)
{
    char string[200];
    if(0 < vsprintf(string,fmt,argp)) // build string
    {
//        HAL_UART_Transmit(&huart2, (uint8_t*)string, strlen(string), 0xffffff); // send message via UART
    }
}

void art_printf(const char *fmt, ...) // custom printf() function
{
    va_list argp;
    va_start(argp, fmt);
    vprint(fmt, argp);
    va_end(argp);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	#if GESTURE_PRESENT
		static int32_t toggle1 = 0;
		static int32_t toggle2 = 0;
		static int32_t led_counter = 63;
		static int32_t scene_trigger = 0;
	#endif


	#if TEST_STATIC
		return;
	#endif


    #if GESTURE_PRESENT
	if (GPIO_Pin == GPIO_PIN_4)
	{
		//gest CW
		led_color_t color;

		if (toggle1 == 0)
		{
			led_debug_set_green();
			toggle1 = 1;
		}
		else
		{
			led_debug_clr_green();
			toggle1 = 0;
		}

		//if (!is_play_scene())
		//if (scene_trigger < 2)
		{
			color.r = 0;
			color.g = 0;
			color.b = 0;
			led_set_color_pure(led_counter, color, LED_PRIORITY_GEST);
			led_drive(led_counter, LED_GEST_CW_ID);

			led_counter--;
			if (led_counter<0)
			{
				led_counter = 63;
				scene_trigger++;
				if (scene_trigger==2)
				{
					play_scene();
					//return;
				}
			}

			color.r = 255;
			color.g = 255;
			color.b = 255;
			led_set_color_gamma(led_counter, color, LED_PRIORITY_GEST);
			led_drive(led_counter, LED_GEST_CW_ID);
		}
	}

	if (GPIO_Pin == GPIO_PIN_0)
	{
		//gest CCW
		led_color_t color;

		if (toggle2 == 0)
		{
			led_debug_set_red();
			toggle2 = 1;
		}
		else
		{
			led_debug_clr_red();
			toggle2 = 0;
		}


		//if (!is_play_scene())
		//if (scene_trigger < 2)
		{
			color.r = 0;
			color.g = 0;
			color.b = 0;
			led_set_color_pure(led_counter, color, LED_PRIORITY_GEST);
			led_drive(led_counter, LED_GEST_CCW_ID);

			led_counter++;
			if (led_counter>63)
			{
				led_counter = 0;
			}

			color.r = 255;
			color.g = 255;
			color.b = 255;
			led_set_color_gamma(led_counter, color, LED_PRIORITY_GEST);
			led_drive(led_counter, LED_GEST_CCW_ID);
		}
	}
    #endif

    #if TOUCH_LEFT_PRESENT
	if (GPIO_Pin == GPIO_PIN_2)
	{
		//touch_1 (left)
		led_color_t color;

		if (HAL_GPIO_ReadPin(EXTI2_TOUCH2_INT_GPIO_Port, EXTI2_TOUCH2_INT_Pin) == GPIO_PIN_RESET)
		{
			color.r = 255;
			color.g = 0;
			color.b = 0;
			pwm_led_set(LED_PWM_1, TOUCH_LED_FULL);
		}
		else
		{
			color.r = 0;
			color.g = 0;
			color.b = 0;
			pwm_led_set(LED_PWM_1, TOUCH_LED_DARK);
		}

		led_set_color_pure(16, color, LED_PRIORITY_TOUCH);
		led_set_color_pure(17, color, LED_PRIORITY_TOUCH);
		led_set_color_pure(18, color, LED_PRIORITY_TOUCH);
		led_set_color_pure(19, color, LED_PRIORITY_TOUCH);
		led_set_color_pure(20, color, LED_PRIORITY_TOUCH);
		led_drive(16, LED_TOUCH_L_ID);
		led_drive(17, LED_TOUCH_L_ID);
		led_drive(18, LED_TOUCH_L_ID);
		led_drive(19, LED_TOUCH_L_ID);
		led_drive(20, LED_TOUCH_L_ID);
	}
    #endif

    #if TOUCH_RIGHT_PRESENT
	if (GPIO_Pin == GPIO_PIN_8)
	{
		//touch_2 (right)
		led_color_t color;

		if (HAL_GPIO_ReadPin(EXTI8_TOUCH1_INT_GPIO_Port, EXTI8_TOUCH1_INT_Pin) == GPIO_PIN_RESET)
		{
			color.r = 255;
			color.g = 0;
			color.b = 0;
			pwm_led_set(LED_PWM_2, TOUCH_LED_FULL);
		}
		else
		{
			color.r = 0;
			color.g = 0;
			color.b = 0;
			pwm_led_set(LED_PWM_2, TOUCH_LED_DARK);
		}

		led_set_color_pure(43, color, LED_PRIORITY_TOUCH);
		led_set_color_pure(44, color, LED_PRIORITY_TOUCH);
		led_set_color_pure(45, color, LED_PRIORITY_TOUCH);
		led_set_color_pure(46, color, LED_PRIORITY_TOUCH);
		led_set_color_pure(47, color, LED_PRIORITY_TOUCH);
		led_drive(43, LED_TOUCH_R_ID);
		led_drive(44, LED_TOUCH_R_ID);
		led_drive(45, LED_TOUCH_R_ID);
		led_drive(46, LED_TOUCH_R_ID);
		led_drive(47, LED_TOUCH_R_ID);
	}
    #endif

    #if ACC_PRESENT
	if (GPIO_Pin == GPIO_PIN_5)
	{
		// acc
		led_color_t color;

		if (HAL_GPIO_ReadPin(EXTI5_ACC_INT_GPIO_Port, EXTI5_ACC_INT_Pin) == GPIO_PIN_RESET)
		{
			color.r = 0;
			color.g = 0;
			color.b = 0;
		}
		else
		{
			color.r = 100;
			color.g = 50;
			color.b = 100;
		}

		led_set_color_pure(30, color, LED_PRIORITY_ACC);
		led_set_color_pure(31, color, LED_PRIORITY_ACC);
		led_set_color_pure(32, color, LED_PRIORITY_ACC);
		led_set_color_pure(33, color, LED_PRIORITY_ACC);
		led_drive(30, LED_ACC_ID);
		led_drive(31, LED_ACC_ID);
		led_drive(32, LED_ACC_ID);
		led_drive(33, LED_ACC_ID);

	}
	#endif

}


void HAL_TIM6_Callback(void)
{
	if (is_play_scene())
	{
		execute_active_scene(get_temperature_value(), get_light_value());
	}

	//if (is_update_temperature())
	//{
	//	execute_update_temperature();
	//}

	//if (is_update_light())
	//{
	//	execute_update_light();
	//}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	#if POLO_AL_BOARD_DMX
		if (huart->ErrorCode == HAL_UART_ERROR_FE)						//detekcja MAB
		{
			HAL_TIM_Base_Start_IT(&htim15);
		}

		if(uart_active_start == false)
		{

			for (int i=0; i < USED_DMX_SLOTS; i++)
			{
				DMXframe.DMXInputData[i] = DMXframe.RecBuf[DMXStartAddress + i];
			}

			uart_active_start = true;

			for (int i = 0; i < 4500000; i++)
			{
				i++;
			}

		}
	#endif
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{


}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) ////////////////////////////// *PM* COŚ ROBI BO WYKRYLO RAMKE
{
	#if POLO_AL_BOARD_DMX
		static uint8_t one_occur = 0;
		static uint8_t break_bits_counter = 0;

		if (__HAL_TIM_GET_IT_SOURCE(&htim15, TIM_IT_UPDATE) != RESET)
		{

			if (break_bits_counter == 0)
				one_occur = 0;

			break_bits_counter++;

			if (HAL_GPIO_ReadPin(USART2_RX_GPIO_Port, USART2_RX_Pin) == GPIO_PIN_SET &&
				(break_bits_counter < DMX_BREAK_TIME))		//probkowanie RxD WE //
			{
				HAL_TIM_Base_Stop_IT(&htim15);
				one_occur = 1;
				break_bits_counter = 0;
			}

			if (break_bits_counter >= DMX_BREAK_TIME && (one_occur == 0))
			{
				uart_active_start = false;
				HAL_UART_Receive_IT(&huart2, DMXframe.RecBuf, SERIAL_LENGTH);

				break_bits_counter = 0;
				HAL_TIM_Base_Stop_IT(&htim15);
			}

			__HAL_TIM_CLEAR_IT(&htim15, TIM_IT_UPDATE);

		/* Prevent unused argument(s) compilation warning */
		}
	#endif
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
