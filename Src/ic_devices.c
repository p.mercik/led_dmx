/*
 * ic_devices.c
 *
 *  Created on: 21.12.2018
 *      Author: PLARZAW1
 */


#include "ic_devices.h"



//leds
#if LED_GROUP_PRESENT_0
	ic_address_t lp5024_0_addr =  { .interface = INTERFACE_I2C_1,  .address = LP5024_ADDR_0 };
#endif
#if LED_GROUP_PRESENT_1
	ic_address_t lp5024_1_addr =  { .interface = INTERFACE_I2C_1,  .address = LP5024_ADDR_1 };
#endif
#if LED_GROUP_PRESENT_2
	ic_address_t lp5024_2_addr =  { .interface = INTERFACE_I2C_1,  .address = LP5024_ADDR_2 };
#endif
#if LED_GROUP_PRESENT_3
	ic_address_t lp5024_3_addr =  { .interface = INTERFACE_I2C_1,  .address = LP5024_ADDR_3 };
#endif
#if LED_GROUP_PRESENT_4
	ic_address_t lp5024_4_addr =  { .interface = INTERFACE_I2C_2,  .address = LP5024_ADDR_4 };
#endif
#if LED_GROUP_PRESENT_5
	ic_address_t lp5024_5_addr =  { .interface = INTERFACE_I2C_2,  .address = LP5024_ADDR_5 };
#endif
#if LED_GROUP_PRESENT_6
	ic_address_t lp5024_6_addr =  { .interface = INTERFACE_I2C_2,  .address = LP5024_ADDR_6 };
#endif
#if LED_GROUP_PRESENT_7
	ic_address_t lp5024_7_addr =  { .interface = INTERFACE_I2C_2,  .address = LP5024_ADDR_7 };
#endif


//touch
#if TOUCH_LEFT_PRESENT
	ic_address_t mtch112_1_addr = { .interface = INTERFACE_I2C_1,  .address = MTCH112_ADDR_1 };
#endif
#if TOUCH_RIGHT_PRESENT
	ic_address_t mtch112_2_addr = { .interface = INTERFACE_I2C_2,  .address = MTCH112_ADDR_2 };
#endif

//gest
#if GESTURE_PRESENT
	ic_address_t mgc3130_addr =   { .interface = INTERFACE_I2C_1,  .address = MGC3130_ADDR };
#endif

//accelerometer
#if ACC_PRESENT
	ic_address_t lis2dh12_addr =  { .interface = INTERFACE_I2C_1,  .address = LIS2DH12_ADDR };
#endif

//light
#if LIGHT_PRESENT
	ic_address_t tsl2569_addr =   { .interface = INTERFACE_I2C_2,  .address = TSL2569_ADDR };
#endif

//temperature
#if TEMPERATURE_PRESENT
	ic_address_t at30ts74_addr =  { .interface = INTERFACE_I2C_1,  .address = AT30TS74_ADDR };
#endif


